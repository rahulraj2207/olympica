import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sportsgram/firebase/signUpData.dart';
import 'package:sportsgram/owner/ownerLogin.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/screens/Auth/Screens/loginScreen.dart';
import 'package:sportsgram/utils/utils/assets/assetsUtils.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';

String key;
class OwnerProfile extends StatefulWidget {

  OwnerProfile(
      {
        this.emaill,
      });
  String emaill;

  @override
  _OwnerProfileState createState() => _OwnerProfileState();
}

class _OwnerProfileState extends State<OwnerProfile> {


  TextEditingController reviewcontroller = TextEditingController();



  final ScrollController scrollController = ScrollController();


  var userList = List<SignUpData>().obs;


  void getUSER(String emm)async {
    if (await SignUpStore().database1(emm) != null) {

      for (var user in await SignUpStore().getFromDb1(emm)) {
        userList.add(user);
      }
    }
  }

  final ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    getUSER(widget.emaill);
    print(userList.length);
    setState(() {

    });
    super.initState();
  }



  @override
  Widget build(BuildContext context) {


    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: WhiteAppBar(
        title: "Profile",
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: size.height/3,
                  width: size.width,
                  child: Obx(
                        () {

                      if (userList.isEmpty)

                        return Center(child: CircularProgressIndicator(color: Colors.green,));
                      else
                        return ListView.builder(
                          controller: _scrollController,
                          padding: const EdgeInsets.only(bottom: 25, top: 15),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemCount: userList.length,
                          itemBuilder: (context, i)
                          {

                            key = userList[i].key;


                            return Padding(
                              padding:   EdgeInsets.only(left: 20,right: 20,bottom: 25),

                              child: Profilecard1(
                                username: userList[i].username,
                                name: userList[i].name,
                                surname: userList[i].surname,
                                emai: userList[i].email1,
                              ),

                            );


                          },
                        );
                    },
                  ),
                ),


                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: ListTile(
                    contentPadding: EdgeInsets.only(left: 5,right: 5),
                    trailing: roundedIconBox(
                        icon: AssetsUtils.logout,
                        bgColor: Colors.green.withOpacity(0.2),
                        iconColor: Colors.green.shade900,
                        width: 40,
                        height: 40),
                    leading: Text(
                      "Log out",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Get.textTheme.headline5.copyWith(color: cBlack.withOpacity(.5), fontSize: 22,fontWeight: FontWeight.w500),
                    ),
                    onTap: ()async{

                      SharedPreferences prefs = await SharedPreferences.getInstance();
                      prefs.remove('islogged');
                      Get.to(OwnerLogin());


                    },
                  ),
                ),


              ],
            ),
          ),
        ),
      ),
    );
  }
}





class Profilecard1 extends StatelessWidget {
  final String username;
  final String name;
  final String surname;
  final String emai;

  const Profilecard1(
      {Key key,
        this.username,
        this.name,
        this.surname,
        this.emai})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SizedBox(
      height: 250,

      width: size.width,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Divider(thickness: 1),


            Row(
              children: [

                Center(
                  child: Text(
                      "User Name: ",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(.3),
                        fontSize: 20,
                      )
                  ),
                ),

                Center(
                  child: Text(
                      username??"",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(.6),
                        fontSize: 20,
                      )
                  ),
                ),



              ],
            ),
            Divider(thickness: 1),


            Row(
              children: [
                Center(
                  child: Text(
                      "Name:          ",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(.3),
                        fontSize: 20,
                      )
                  ),
                ),

                Text(
                  name ??"",
                  textAlign: TextAlign.left,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black.withOpacity(.6),
                  ),
                ),
                SizedBox(width: 5),
                Text(
                  surname??"",
                  textAlign: TextAlign.left,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.black.withOpacity(.6),
                  ),
                ),

              ],
            ),

            Divider(thickness: 1),

Row(
  children: [
    Center(
      child: Text(
          "Email:          ",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black.withOpacity(.3),
            fontSize: 20,
          )
      ),
    ),

    Center(
      child: Text(
        emai??"",
        textAlign: TextAlign.left,
        style: Get.textTheme.subtitle1.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 20,
          color: Colors.black.withOpacity(.6),
        ),
      ),
    ),

  ],
),

            Divider(thickness: 3,),

          ],
        ),
      ),
    );
  }
}



