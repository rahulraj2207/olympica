import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/owner/ownerSignUp.dart';


class OwnerSignUpNoteBody extends StatefulWidget {
  const OwnerSignUpNoteBody({
    Key key,
  }) : super(key: key);

  @override
  _OwnerSignUpNoteBodyState createState() => _OwnerSignUpNoteBodyState();
}

class _OwnerSignUpNoteBodyState extends State<OwnerSignUpNoteBody> {
  TapGestureRecognizer onTapGD;

  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      print("completed");
      setState(() {});
    });


    super.initState();
    onTapGD = TapGestureRecognizer()..onTap = _signUp;
  }

  @override
  void dispose() {
    onTapGD.dispose();
    super.dispose();
  }

  void _signUp() {
    Get.focusScope.unfocus();
    Get.to(
      OwnerSignUp(),
      transition: Transition.downToUp,
    );
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Not on Olympica yet? ',
          style: TextStyle(fontSize: 17,color:  Colors.white),
          children: [
            TextSpan(
              recognizer: onTapGD,
              text: 'Sign up',
              style: TextStyle(fontSize: 19,fontWeight: FontWeight.w700,color: Colors.white),
            )
          ]),
    );
  }
}
