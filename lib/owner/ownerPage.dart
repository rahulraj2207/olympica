

import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:get/get.dart';
import 'package:sportsgram/owner/ownerAccount.dart';
import 'package:sportsgram/utils/utils/widgets.dart';



int vehicleid;
List<Ground>groundList1=[];

class OwnerPage extends StatefulWidget {

  OwnerPage(
      {
        this.img,
        this.gname,
        this.gdet,
        this.gab,
        this.gloc,


      });
  File img;
  String gname;
  String gdet;
  String gab;
  String gloc;







  // TurfPage({required this.id});
  String id;

  @override
  _OwnerPageState createState() => _OwnerPageState();
}

class _OwnerPageState extends State<OwnerPage> {


  TextEditingController reviewcontroller = TextEditingController();
  String ratings = '';
  GlobalKey<ScaffoldState> key = GlobalKey();
  bool savecolor = false;


  // insertG(){
  //   var insrt=Ground(gimage:widget.img,gname: widget.gname,gdetails: widget.gdet,gLOc: widget.gloc,gabout: widget.gab);
  //   GroundStore().setToDatabase(insrt);
  //
  //   // Navigator.pop(context);
  // }
@override
  void initState() {


// insertG();


  super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return FutureBuilder(
      // future: groundList[0].gname,
        builder: (context, snapshot) {
            return Scaffold(
              appBar: AppBar(
                leading: InkWell(
                  onTap: () {
                    Get.offAll(OwnerAccount());
                  },
                  child: Icon(
                    FontAwesomeIcons.arrowLeft,
                    color: Color(0xff41287B),
                    size: 20,
                  ),
                ),
                backgroundColor: Colors.white,

              ),
              body:  Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      // borderRadius: BorderRadius.circular(15)
                    ),
                    height: 200,
                    width: size.width,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: Image.file(widget.img??"",
                        // snapshot.data!.details.images[0],
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 25,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(widget.gname??"",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                  color: Color(
                                    0xff41287B,
                                  ),
                                ),
                              ),
                            ),
                            // SizedBox(
                            //   width: 10,
                            // ),
                            // Container(
                            //   width: 120,
                            //   height: 35,
                            //   decoration: BoxDecoration(
                            //     color: Color(0xffFF9E40),
                            //     borderRadius: BorderRadius.circular(8),
                            //   ),
                            //   child: MaterialButton(
                            //     onPressed: () {
                            //       TurnOn("Accept Bookings");
                            //       // Get.to(BookPage1());
                            //
                            //     },
                            //     child: Text(
                            //       'Accept',
                            //       style: TextStyle(
                            //         color: Color(
                            //           0xff41287B,
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // )
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.location_on,
                                size: 25,
                                color: Color(0xffFF9E40),
                              ),
                              SizedBox(
                                width: 7,
                              ),
                              Text(widget.gloc??"",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Color(
                                    0xff41287B,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Divider(
                          color: Color(0xffFF9E40),
                        ),
                        Text(
                          "Contact",style: TextStyle(
                          fontSize: 20,fontWeight: FontWeight.w500,
                          color: Colors.black.withOpacity(.5)
                        ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          widget.gdet??"",style: TextStyle(
                          fontSize: 20,
                          color: Color(
                            0xff41287B,
                          ),
                        ),
                        ),

                        SizedBox(
                          height: 10,
                        ),

                        Divider(
                          color: Color(0xffFF9E40),
                        ),
                        Text(
                          "Category",style: TextStyle(
              fontSize: 20,fontWeight: FontWeight.w500,
              color: Colors.black.withOpacity(.5)
              ),
                        ),
                        SizedBox(
                          height: 10,
                        ),


                        Text(
                          widget.gab??"",style: TextStyle(
                          fontSize: 20,
                          color: Color(
                            0xff41287B,
                          ),
                        ),
                        ),
                        Divider(
                          color: Color(0xffFF9E40),
                        ),


                        Wrap(
                          children: [
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              width: (MediaQuery
                                  .of(context)
                                  .size
                                  .width / 3) - 30,
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.handsWash,
                                    size: 18,
                                    color: Color(0xffFF9E40),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'WASHROOM',
                                      style: TextStyle(
                                          color: Color(
                                            0xff41287B,
                                          ),
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              width: (MediaQuery
                                  .of(context)
                                  .size
                                  .width / 3) - 30,
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.handsWash,
                                    size: 18,
                                    color: Color(0xffFF9E40),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'CHANGING ROOM',
                                      style: TextStyle(
                                          color: Color(
                                            0xff41287B,
                                          ),
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              width: (MediaQuery
                                  .of(context)
                                  .size
                                  .width / 3) - 30,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.local_drink_outlined,
                                    size: 18,
                                    color: Color(0xffFF9E40),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'DRINKING WATER',
                                      style: TextStyle(
                                          color: Color(
                                            0xff41287B,
                                          ),
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              width: (MediaQuery
                                  .of(context)
                                  .size
                                  .width / 3) - 30,
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.parking,
                                    size: 18,
                                    color: Color(0xffFF9E40),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'PARKING',
                                      style: TextStyle(
                                          color: Color(
                                            0xff41287B,
                                          ),
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 5),
                              width: (MediaQuery
                                  .of(context)
                                  .size
                                  .width / 3) - 30,
                              child: Row(
                                children: [
                                  Icon(
                                    FontAwesomeIcons.firstAid,
                                    size: 18,
                                    color: Color(0xffFF9E40),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                    child: Text(
                                      'FIRST AID',
                                      style: TextStyle(
                                          color: Color(
                                            0xff41287B,
                                          ),
                                          fontSize: 10),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(
                          color: Color(0xffFF9E40),
                        ),

                        SizedBox(
                          height: 10,
                        ),

                      ],
                    ),
                  ),
                ],
              )


              );


        });
  }
}

Future<void> TurnOn(String error) {
  botSuccessMsg(sucessMsg: error, isSuccess: true);
  return Future.delayed(const Duration(seconds: 3))
      .then((value) => BotToast.closeAllLoading());
}