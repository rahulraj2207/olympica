import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/screens/Auth/widgets/ratingStar.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/controller/homePageController.dart';



int max = 4;

class  GroundDetails extends StatefulWidget {


  GroundDetails(
      {
        this.email,
      });
  final String email;




  @override
  _GroundDetailsState createState() => _GroundDetailsState();
}

class _GroundDetailsState extends State<GroundDetails> {


  // final GetListControllerO getListController = Get.put(GetListControllerO(email: widget.email));

  var ownerList = List<Ground>().obs;


  void getOwnergroundList(String em)async {
    if (await GroundStore().databaseO(em) != null) {

      for (var grounds in await GroundStore().getFromDBO(em)) {
        ownerList.add(grounds);
      }
    }
  }


  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
getOwnergroundList(widget.email);
super.initState();
  }


  @override
  Widget build(BuildContext context) {


    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Scaffold(
        appBar: WhiteAppBar(
          title: "Your grounds"
        ),
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        body: SafeArea(
          child: GestureDetector(
            onTap: () {
              Get.focusScope.unfocus();
            },
            child: SingleChildScrollView(
              controller: Get.find<HomePageController>().searchScrollController,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: size.height * 0.03,
                  ),

                  Container(
                    height: size.height/1.15,
                    width: size.width,
                    child: Obx(
                          () {

                        if (ownerList.isEmpty)

                          return Center(child: Text("please register your ground"));
                        else
                          return ListView.builder(
                            controller: _scrollController,
                            padding: const EdgeInsets.only(bottom: 25, top: 15),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: ownerList.length,
                            itemBuilder: (context, i)
                            {
                              int randomNumber = Random().nextInt(max) + 1;

                              return Padding(
                                padding:   EdgeInsets.only(left: 20,right: 20,bottom: 25),

                                child: EventsCard(
                                  eventHeaderImage: ownerList[i].gimage,
                                  about: ownerList[i].gabout,
                                  details: ownerList[i].gdetails,
                                  groundName: ownerList[i].gname,
                                  eventRating: randomNumber,
                                  onpressed: (){

                                  },

                                ),

                              );


                            },
                          );
                      },
                    ),
                  ),

                  SizedBox(
                    height: 100,
                    width: size.width,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


}


class EventsCard extends StatefulWidget {
  final String eventHeaderImage;
  final String about;
  final String details;
  final String groundName;
  final int eventRating;
  final Function() onpressed;

  const EventsCard(
      {Key key,
        this.eventHeaderImage,
        this.about,
        this.onpressed,
        this.details,
        this.eventRating,
        this.groundName})
      : super(key: key);

  @override
  _EventsCardState createState() => _EventsCardState();
}

class _EventsCardState extends State<EventsCard> {
  @override
  Widget build(BuildContext context) {
    var height = 330;

    return SizedBox(
        height: height.toDouble(),
        width: 230,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.5),
                spreadRadius: 6,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (widget.eventHeaderImage != null)
                        Image.network(
                          widget.eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),

                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Icon(CupertinoIcons.phone),
                            const SizedBox(width: 4),
                            Expanded(
                                child: buildCaptions(
                                    text:widget.details??"",
                                    // '$profileName • ${DateFormat.yMMMMd().format(profileEventsStartDate)}',
                                    alignment: Alignment.centerLeft))
                          ],
                        ),
                        Text(
                          widget.groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        Text(
                          widget.about??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),



                        Row(mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Rating",style: TextStyle(
                                fontSize: 15
                            ),
                            ),

                            Expanded(
                              child: RatingWithStar(
                                rating: widget.eventRating ?? 0,
                              ),
                            ),

                          ],
                        ),


                        // Align(
                        //   alignment: Alignment.bottomRight,
                        //   child: SizedBox(height: 27,width: 100,
                        //     child:
                        //     TextButton(onPressed: widget.onpressed,
                        //         style: TextButton.styleFrom(
                        //             backgroundColor:  Colors.red,
                        //             primary: Colors.white,
                        //
                        //             side: BorderSide(color: Colors.green,width: 0.01,)
                        //         ),
                        //         child: Text(
                        //         'Remove',style: TextStyle(fontSize: 12,fontWeight: FontWeight.w900),
                        //         )),
                        //   ),
                        // )


                        // )
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
