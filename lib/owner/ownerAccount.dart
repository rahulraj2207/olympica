import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/owner/groundDetails.dart';
import 'package:sportsgram/owner/ownerLogin.dart';
import 'package:sportsgram/owner/ownerProfile.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/owner/addGround.dart';

import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/utils/utils/assets/assetsUtils.dart';
String userEmail;


class OwnerAccount extends StatefulWidget {



  @override
  _OwnerAccountState createState() => _OwnerAccountState();
}

class _OwnerAccountState extends State<OwnerAccount> {




  final _auth = FirebaseAuth.instance;

  User loggedInUser;

  void getCurrentUseronMatches()async{
    try{
      final user = await _auth.currentUser;
      if(user!= null){
        loggedInUser = user;
        String el = loggedInUser.email;
        List uE = el.split("@");
        userEmail= uE[0];
      }
    }catch(e){
      print(e);
    }

  }

@override
  void initState() {
  getCurrentUseronMatches();
  print(userEmail);
  super.initState();
  }







  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    var sizedBox002 = SizedBox(height: size.height * 0.02);

    return Scaffold(
      appBar: buildAppBar(
        title: Text('Owner Account',style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
        ),),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(25),
        child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [


            Divider(thickness: 1,),

            buildIconTile(
                icon: AssetsUtils.iniviteUser,
                text: 'Add grounds',

                onPressed: () {
                  Get.to(AddGRound(email: userEmail),
                      transition: Transition.cupertino);
                }),
Divider(thickness: 1,),

            sizedBox002,
            Divider(thickness: 1,),

            buildIconTile(
                icon: AssetsUtils.share,
                text: 'Your Grounds',

                onPressed: () {
                  Get.to(GroundDetails(email: userEmail),
                      transition: Transition.cupertino);
                }),
            Divider(thickness: 1,),

            sizedBox002,

            Divider(thickness: 1,),

            buildIconTile(
                icon: AssetsUtils.personalInfo,
                text: 'Profile',

                onPressed: () {
                  Get.to(OwnerProfile(emaill: userEmail,));


                }),
            Divider(thickness: 1,),

            sizedBox002,



          ],
        ),
      ),
    );
  }
}
