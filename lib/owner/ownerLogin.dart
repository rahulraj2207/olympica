import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/admin/adminLogin.dart';
import 'package:sportsgram/controller/UserDataController.dart';
import 'package:sportsgram/screens/Auth/Screens/loginScreen.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/screens/Auth/widgets/myTextField2.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:sportsgram/ownerController/ownerLoginController.dart';
import '../screens/Auth/Screens/forgotPasswordScreen.dart';
import 'package:sportsgram/owner/ownerSignUpNoteBody.dart';

class OwnerLogin extends StatefulWidget {
  @override
  _OwnerLoginState createState() => _OwnerLoginState();
}



class _OwnerLoginState extends State<OwnerLogin> {

  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      print("completed");
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Get.put(UserDataController());

    return GestureDetector(
      onTap: () {
        if (Get.focusScope.hasFocus) {
          Get.focusScope.unfocus();
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: Get.width,
            height: Get.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg4.jpeg"),
                fit: BoxFit.cover,
              ),
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [


                    Align(alignment: Alignment.topRight,child: Text("User",style: TextStyle(
                      color: Colors.black,fontSize: 15,fontWeight: FontWeight.w900

                    ),)),
                    SizedBox(height: Get.height * 0.01),


                    Align(  alignment: Alignment.topRight,
                      child: GestureDetector(
                          onTap: (){
                            Get.to(LoginScreen(),
                                duration: const Duration(milliseconds: 600),
                            curve: Curves.easeInOutSine,
                            transition: Transition.upToDown,);

                          },

                          child: Icon(Icons.switch_account_sharp,color: Colors.black,)),
                    ),

                    SizedBox(height: Get.height * 0.10),


                    buildheadings(text: 'Owner Login', height: 45,style: TextStyle(
                      fontSize: 50, color: Colors.white,fontWeight: FontWeight.bold,
                    )),
                    SizedBox(height: Get.height * 0.03),

                    // Center(
                    //   child:
                    //   // Image.asset(
                    //   //   'assets/images/sportslogo.png',
                    //   //   width: 200,
                    //   // ),
                    // ),
                    // SizedBox(height: Get.height * 0.15),
                    Row(
                      children: [
                        Expanded(
                          child: MyTextFiled1(
                            enums: TextFieldSwitchers1.lEmail,
                            hintText: 'Email',
                          ),
                        ),
                        GetX<OwnerLoginController>(
                          builder: (controller) => buildErrorIcon(
                              onPressed: () {
                                BotToast.showText(
                                    text: controller.emailUserMobError.value,
                                    align: Alignment.center
                                );
                              },
                              errorMsg: controller.emailUserMobError.value
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: Get.height * 0.03),
                    Row(
                      children: [
                        Expanded(
                          child: MyTextFiled1(
                            enums: TextFieldSwitchers1.lPassword,
                            hintText: 'Password',
                          ),
                        ),
                        GetX<OwnerLoginController>(
                          builder: (controller) => buildErrorIcon(
                              onPressed: () {
                                BotToast.showText(
                                    text: controller.passwordError.value,
                                    align: Alignment.center
                                );
                              },
                              errorMsg: controller.passwordError.value
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: Get.height * 0.03),

                    Align(
                      alignment: Alignment.center,
                      child: buildMyButton(
                        onpressed: () {
                          Get.focusScope.unfocus();
                          Get.find<OwnerLoginController>().validateRahul1();
                        },
                        text: 'Sign in',
                        width: Get.width > 600 ? 550 : Get.width,
                        height: 60,
                      ),
                    ),
                    SizedBox(height: Get.height * 0.03),

                    Align(alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () {
                          Get.focusScope.unfocus();
                          Get.to(
                            ForgotPasswordScreen(),
                            transition: Transition.leftToRightWithFade,
                          );
                        },
                        child: Text(
                          'Forgot Password?',
                          style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold,color: Colors.white.withOpacity(.8)),
                        ),
                      ),
                    ),
                    SizedBox(height: Get.height * 0.09),
                    const Align(
                      alignment: Alignment.bottomCenter,
                      child: OwnerSignUpNoteBody(),
                    ),


                    SizedBox(height: Get.height * 0.09),

                    // GestureDetector(onTap:(){
                    //   Get.to(AdminLogin());
                    //
                    // },child: Align( alignment: Alignment.bottomCenter,
                    //   child: Text("Switch to Admin",style: TextStyle(
                    //     fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white
                    //   ),),
                    // )),


                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}



