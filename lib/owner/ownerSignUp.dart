import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/widgets/myTextField2.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:sportsgram/ownerController/ownerSignUpController.dart';

class OwnerSignUp extends StatefulWidget {
  @override
  _OwnerSignUpState createState() => _OwnerSignUpState();
}

class _OwnerSignUpState extends State<OwnerSignUp> {
  Future<bool> _willPop() async {
    final signUpCtr = Get.find<OwnerSignUpController>();

    if (signUpCtr.isLoadimg) {
      return false;
    } else {
      return true;
    }
  }
  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      print("completed");
      setState(() {});
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    Get.put(OwnerSignUpController());
    return GestureDetector(
      onTap: () {
        if (Get.focusScope.hasFocus) {
          Get.focusScope.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: _willPop,
        child: Scaffold(
          appBar: buildAppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_rounded),
              onPressed: () => Get.close(1),
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Enter Your Details',style: TextStyle(
                  fontWeight: FontWeight.bold,fontSize: 30,
                ),),
                SizedBox(
                  height: Get.height * 0.05,
                ),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled1(
                          enums: TextFieldSwitchers1.sFirstName,
                          hintText: 'Owner Name'),
                    ),
                    GetX<OwnerSignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.nameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.nameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled1(
                        enums: TextFieldSwitchers1.sSurname,
                        hintText: 'Surname',
                      ),
                    ),
                    GetX<OwnerSignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: 'surname',
                                // controller.surnameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.surnameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                // const PhoneNumberWidget(),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled1(
                          enums: TextFieldSwitchers1.sEmail,
                          hintText: 'Email address'),
                    ),
                    GetX<OwnerSignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.emailError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.emailError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled1(
                          enums: TextFieldSwitchers1.sUserName,
                          hintText: 'Username'),
                    ),
                    GetX<OwnerSignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.userNameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.userNameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled1(
                          enums: TextFieldSwitchers1.sPassword,
                          hintText: 'Password'),
                    ),
                    GetX<OwnerSignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.passwordError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.passwordError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                SizedBox(height: Get.height * 0.01),
                SizedBox(height: Get.height * 0.03),
                const SignUpButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SignUpButton extends StatelessWidget {
  const SignUpButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder
    <OwnerSignUpController>
      (
      builder: (ctr) => Align(
        alignment: Alignment.center,
        child: buildMyButton(
          onpressed:

              () {
            {
              Get.focusScope.unfocus();
              Get.find<OwnerSignUpController>().validateRahul();

              // Get.to(OwnerAccount(),
              //   duration: const Duration(milliseconds: 600),
              //   curve: Curves.easeInOutSine,
              //   transition: Transition.downToUp,
              // );
              // signUpSuccessToast('SignUp Successful');
            }
          },
          text: 'Continue',
          width: Get.width > 600 ? 550 : Get.width,
          height: 60,
        ),
      ),
    );
  }
}

void signUpSuccessToast(String msg) {
  botSuccessMsg(sucessMsg: msg, isSuccess: true);
  Future.delayed(const Duration(seconds: 3))
      .then((value) => BotToast.closeAllLoading());
}