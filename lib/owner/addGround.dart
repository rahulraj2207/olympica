import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sportsgram/controller/app_state_controller.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/owner/ownerPage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;
import 'package:sportsgram/owner/map.dart';


File imageG;
PickedFile _image1;
String uploadedFileURLG;


List<String> sport1 = ['Cricket',
  'Football',
  'Basketball',
  'Volleyball',
  'Table tennis',
  'Tennis',
  'Badminton',
  'Chess',
  'Dodgeball',
  'Hockey']; // Option 2
String selectedSport1;


List time=[];




class AddGRound extends StatefulWidget {


  AddGRound(
      {
        this.email,
      });
  final String email;


  @override
  _AddGRoundState createState() => _AddGRoundState();
}

class _AddGRoundState extends State<AddGRound> {



  Future uploadFile() async {
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('_uploadedFileURLG/${Path.basename(imageG.path)}}');
    UploadTask uploadTask = storageReference.putFile(imageG);
    await uploadTask;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        uploadedFileURLG = fileURL;
        print("hihi");
        print(uploadedFileURLG);
      });
    });
  }



  Position _currentPosition;
  String _currentAddress;


  final TextEditingController grndnamectr = TextEditingController();

  final TextEditingController locctr = TextEditingController();

  final TextEditingController detctr = TextEditingController();

  final TextEditingController aboutctr = TextEditingController();


  final TextEditingController openctr = TextEditingController();

  final TextEditingController closectr = TextEditingController();


  int vehicleid;

  List<Ground>groundList=[];


 double groundCurrentLocationLat;
  double groundCurrentLocationLong;


  _getCurrentLocation()async {
   Position pos =await _determinePosition();

      setState(() {
        _currentPosition = pos;
        groundCurrentLocationLat = pos.latitude;
                  groundCurrentLocationLong = pos.longitude;
        _getAddressFromLatLng();
      });

  }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {

      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {

        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }



  _getAddressFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentPosition.latitude,
          _currentPosition.longitude
      );

      Placemark place = placemarks[0];

      setState(() {
        _currentAddress = "${place.name}";
      });
    } catch (e) {
      print(e);
    }
  }

@override
  void initState() {
print(widget.email);
super.initState();
  }






  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    var sizedBox002 = SizedBox(height: size.height * 0.02);


    return Scaffold(
      appBar: const WhiteAppBar(
        title: 'Ground Information',
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              sizedBox002,


              PickerG(),
              sizedBox002,
              // Align(alignment: Alignment.center,
              //   child: GestureDetector(onTap:()
              //       {
              //         uploadFile();
              //         print("hi");
              //       }
              //       ,child: Center(child: ImageAdd())),
              // ),

            Align(alignment: Alignment.topRight,
              child: SizedBox(height: 26,width: 100,
                child: TextButton(onPressed: (){

                  BotToast.showText(
                      text: "Image uploaded",
                      align: Alignment.center
                  );


                  uploadFile();

                },
                    style: TextButton.styleFrom(
                        backgroundColor: Colors.green.withOpacity(0.09),
                        primary: Colors.green,

                        side: BorderSide(color: Colors.green,width: 0.01,)
                    ),
                    child: Text(
                      'Add Image',style: TextStyle(fontSize: 8,fontWeight: FontWeight.w900),
                    )),
              ),
            ),


              sizedBox002,
              buildTxtHeading('Ground Name'),
              sizedBox002,
              TextFieldProfileEdit(
                controller: grndnamectr,

              ),
              sizedBox002,

              GestureDetector(
                  onTap: (){
                    Get.bottomSheet(
                      ClipRRect(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        child: ColoredBox(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                ClipRRect(
                                  borderRadius:
                                  BorderRadius.circular(10),
                                  child: ColoredBox(
                                    color: Colors.grey
                                        .withOpacity(0.3),
                                    child: const SizedBox(
                                      height: 5,
                                      width: 40,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets
                                      .symmetric(
                                      vertical: 12.0,
                                      horizontal: 4),
                                  child: Text("Category list",style: TextStyle(
                                      color: Colors.black.withOpacity(.9),fontSize: 20,fontWeight: FontWeight.w500
                                  ),),
                                ),
                                SizedBox(
                                    height: size.height/2.5,
                                    child: ListView.builder(
                                      itemCount: sport1.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return
                                          GestureDetector(onTap: (){
                                            BotToast.showText(
                                                text: sport1[index],
                                                align: Alignment.center
                                            );
                                            setState(() {
                                              print(sport1[index]);
                                              selectedSport1= sport1[index];
                                            });

                                          },
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Container(
                                                height: 50,
                                                width: 50,
                                                color:  Colors.green.withOpacity(.2),

                                                child: Center(
                                                  child: Text(
                                                    sport1[index],style: TextStyle(fontSize: 20),

                                                  ),),),
                                            ),
                                          );
                                      },
                                    )
                                )],
                            ),
                          ),
                        ),
                      ),
                      isScrollControlled: true,
                    );

                  },
                  child :Column(
                    children: [
                      SizedBox(
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTxtHeading("Select Category"),
                            Icon(CupertinoIcons.arrow_right,size: 30,color: Colors.green,),
                          ],
                        ),
                      ),
                      Card(
                        color: cAnccent,
                        child: ListTile(
                          contentPadding: EdgeInsets.only(
                              left: 5,right: 5),
                          leading: Text(
                            selectedSport1??"",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 19),
                          ),
                        ),
                      )
                    ],
                  )
              ),


              sizedBox002,



              buildTxtHeading('Location'),
              sizedBox002,
              ColoredBox(
                  color: cAnccent,
                  child: TextField(
                    readOnly: true,
                    textAlign: TextAlign.start,
                    style:
                        Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 16),
                    autofocus: false,
                    controller: locctr,
                    decoration: InputDecoration(
                      suffixIcon: GestureDetector(onTap:(){
                      _getCurrentLocation();
                      setState(() {

                        locctr.text = _currentAddress;

                      });

                      },child: Icon(CupertinoIcons.location,color: Colors.green.shade900)),
                        border: InputBorder.none,
                        contentPadding:  EdgeInsets.symmetric(horizontal: 10),
                        ),
                  )),
              sizedBox002,

                    // GestureDetector(onTap:(){
                    //
                    //   Get.to(GMap());
                    // },child: Icon(Icons.icecream)),

              buildTxtHeading('Rate per hour'),
              sizedBox002,
              TextFieldProfileEdit(
                controller: openctr,

              ),
              sizedBox002,
              buildTxtHeading('Contact'),
              sizedBox002,
              TextFieldProfileEdit(
                controller: detctr,

              ),
              sizedBox002,

              SizedBox(height: size.height * 0.03),
              GetBuilder<AppStateController>(
                builder: (appState) => buildMyButton(
                    text: 'Update',
                    isScaledown: true,
                    child: uploadedFileURLG== null
                        ? const SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 1,
                      ),
                    )
                        : null,
                    onpressed: uploadedFileURLG!= null

                    ? (){
                      print("llo");



                      insertG();
                      insertU();
                      signUpSuccessToast('Ground Registered');
                      Get.to(OwnerPage(img: imageG,gname: grndnamectr.text,gab: selectedSport1,gdet: detctr.text,gloc: locctr.text));
                      grndnamectr.clear();
                      detctr.clear();
                      locctr.clear();

                    }:(){
                      print("llo1");


                    },
                    borderRadius: BorderRadius.circular(10),
                    height: 60,
                    width: size.width),
              ),
              sizedBox002,
            ],
          )),
    );
  }

  Widget buildTxtHeading(String text) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style: const TextStyle(fontSize: 20),
        ));
  }




  insertG(){

    var insrt11=Ground(open:openctr.text,lat: groundCurrentLocationLat,long: groundCurrentLocationLong,gimage: uploadedFileURLG,gname: grndnamectr.text,gdetails: detctr.text,gLOc: locctr.text,gabout: selectedSport1);
    GroundStore().setToDatabaseO(insrt11,widget.email);
    print("inesretdOwner");

  }
  insertU(){
    var insrt12=Ground(open:openctr.text,lat: groundCurrentLocationLat,long: groundCurrentLocationLong,gimage: uploadedFileURLG,gname: grndnamectr.text,gdetails: detctr.text,gLOc: locctr.text,gabout: selectedSport1);
    GroundStore().setToDatabaseU(insrt12);
    print("inesretdUser");

  }



}







class TextFieldProfileEdit extends StatefulWidget {
  const TextFieldProfileEdit({
    Key key,
    this.maxLines,
    this.minLines,
    this.prefixText,
    this.controller,
    this.text,
    this.maxLength,
    this.obsecureText,
    this.contentPading,
    this.newTextField = false,
    this.height,
    this.hintText,
    this.titleStyle,
  }) : super(key: key);

  final int minLines;
  final int maxLines;
  final int maxLength;
  final String prefixText;
  final TextEditingController controller;
  final String text;
  final String hintText;
  final bool obsecureText;
  final EdgeInsetsGeometry contentPading;
  final bool newTextField;
  final double height;
  final TextStyle titleStyle;
  @override
  _TextFieldProfileEditState createState() => _TextFieldProfileEditState();
}

class _TextFieldProfileEditState extends State<TextFieldProfileEdit> {
  @override
  void initState() {
    super.initState();
    widget.controller.text = widget.text;
  }

  @override
  void didUpdateWidget(TextFieldProfileEdit oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.text != widget.text) {
      widget.controller.text = widget.text;
    }
  }
  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();
  //     widget.controller.text = widget.text;

  // }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: widget.newTextField
          ? TextField(
        controller: widget.controller,
        textAlign: TextAlign.center,
        style: widget.titleStyle ??
            Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 30),
        decoration: InputDecoration(
            border: InputBorder.none, hintText: widget.hintText ?? ''),
      )
          : SizedBox(
        height: widget.height,
        child: ColoredBox(
            color: cAnccent,
            child: TextField(
              autofocus: false,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: widget.contentPading ??
                      const EdgeInsets.symmetric(horizontal: 10),
                  prefixText: widget.prefixText),
              minLines: widget.minLines ?? 1,
              maxLines: widget.maxLines ?? 1,
              controller: widget.controller,
              maxLength: widget.maxLength,
              obscureText: widget.obsecureText ?? false,
            )),
      ),
    );
  }
}



void signUpSuccessToast(String msg) {
  botSuccessMsg(sucessMsg: msg, isSuccess: true);
  Future.delayed(const Duration(seconds: 3))
      .then((value) => BotToast.closeAllLoading());
}



class PickerG extends StatefulWidget {
  @override
  _PickerGState createState() => _PickerGState();
}

class _PickerGState extends State<PickerG> {




  void _imgFromCamera()  async{
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera ,
    );
    setState(() {

      imageG = File(pickedFile.path);


      // _image1 = _image;
    });
    Navigator.pop(context);
  }

  void _imgFromGallery() async{
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery ,
    );
    setState(() {
      imageG = File(pickedFile.path);


      // _image = _image1;
    });

    Navigator.pop(context);
  }


  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }









  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;


    return       Center(
      child: GestureDetector(
        onTap: () {
          _showPicker(context);
          // CreateEvent1();
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: Colors.grey[200],


          ),
          height: size.height/3.8,
          width: size.width/1.3,
          child: imageG != null
              ? Container(
            child: Image.file(
              imageG,
              width: size.width/1.3,
              height: size.height/3.8,
              fit: BoxFit.fitHeight,
            ),
          )
              : Container(
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(50)),
            width: 100,
            height: 100,
            child: Icon(
              Icons.camera_alt,
              color: Colors.grey[900],
            ),
          ),
        ),
      ),);
  }
}


