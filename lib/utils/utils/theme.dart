import 'package:flutter/material.dart';

Color cPrimary = const Color(0xffFF5917);
Color cAnccent = const Color(0xffF4F4F9);
Color cDarkGrey = const Color(0xff7D7D8D);
Color clightGrey = const Color(0xffE2E2F0);
Color clightOrange = const Color(0xffFFF2EC);
Color cWhite = Colors.white;
Color cBlack = const Color(0xff040405);

//!Fonts
String fOpenSans = 'Open Sans';
String fSegoeui = 'Segoeui';
String fProximanova = 'Proximanova';
String fRunda = 'Runda';
String fHelvetica = 'Helvetica';

ThemeData themData = ThemeData(
  primaryColor: cPrimary,
  buttonColor: cPrimary,
  accentColor: cAnccent,
  fontFamily: fSegoeui,
  cursorColor: cPrimary,
  hintColor: cDarkGrey,

  textTheme: TextTheme(
    headline1: TextStyle(
      fontFamily: fOpenSans,
      fontWeight: FontWeight.w600,
      fontSize: 42,
      color: cBlack,
    ),

    subtitle2: TextStyle(
      fontFamily: fOpenSans,
      fontWeight: FontWeight.w600,
      fontSize: 14,
      color: cDarkGrey,
    ),
    headline6: TextStyle(
      fontFamily: fOpenSans,
      fontWeight: FontWeight.w600,
      fontSize: 16,
      color: cPrimary,
    ),
    subtitle1: TextStyle(
      fontFamily: fOpenSans,
      fontWeight: FontWeight.w600,
      fontSize: 16,
      color: cBlack,
    ),
    button: const TextStyle(color: Colors.white, fontSize: 18),
  ),
  scaffoldBackgroundColor: Colors.white,
  iconTheme: IconThemeData(color: cDarkGrey),
);
