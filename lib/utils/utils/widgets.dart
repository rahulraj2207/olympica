import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import 'theme.dart';

SizedBox roundedIconBox(
    {double height,
      double width,
      double iconWidth,
      String icon,
      Color iconColor,
      BoxBorder border,
      Color bgColor}) {
  return SizedBox(
    height: height ?? 50,
    width: width ?? 50,
    child: DecoratedBox(
      decoration: BoxDecoration(
          color: bgColor ?? cPrimary, shape: BoxShape.circle, border: border),
      child: Align(
          child: SvgPicture.asset(
            icon,
            width: iconWidth ?? 18,
            color: iconColor ?? cWhite,
            alignment: Alignment.center,
          )),
    ),
  );
}

InkWell buildIconTile({Function onPressed, String icon, String text}) {
  return InkWell(

    onTap: onPressed,
    child: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              roundedIconBox(
                  icon: icon,
                  bgColor: Colors.green.withOpacity(.3),
                  iconColor: Colors.green.shade900,
                  height: 40,
                  width: 40,
                  iconWidth: 16),
              const SizedBox(
                width: 15,
              ),
              Text(
                text,
                style: TextStyle(
                    fontSize: 20
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),

          Icon(Icons.chevron_right_outlined)

        ],
      ),
    ),
  );
}

Widget buildBgInputBox(
    {@required Widget child,
      double height = 60,
      double width = double.infinity,
      BorderRadiusGeometry borderRadius}) {
  return Container(
    alignment: Alignment.center,
    height: height,
    width: width,
    decoration: BoxDecoration(
      color: cAnccent,
      borderRadius: borderRadius ?? BorderRadius.circular(7),
    ),
    child: child,
  );
}

Widget buildHedingWithSub(
    {BuildContext context,
      String title,
      String caption,
      Function onpressed,
      EdgeInsetsGeometry padding,
      TextStyle style,

    }) {
  final size = MediaQuery.of(context).size;

  return InkWell(
    onTap: onpressed,
    child: SizedBox(
      height: 60,
      width: size.width,
      child: Padding(
        padding: padding,
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: buildheadings(
                      style: style,
                      text: title,
                      height: 25,
                    ),
                  ),
                  const Icon(CupertinoIcons.forward),
                ],
              ),
            ),
            Expanded(child: buildCaptions(text: caption??"", height: 20,textStyle: TextStyle(fontSize: 15,fontWeight: FontWeight.w500)))
          ],
        ),
      ),
    ),
  );
}

SizedBox buildMyButton(
    {Function onpressed,
      String text,
      double width,
      double height,
      Widget child,
      Color color,
      bool isScaledown = false,
      BorderRadiusGeometry borderRadius}) {
  return SizedBox(
    height: height,
    width: width,
    child: RaisedButton(
      color: color??Colors.green,
      elevation: 0,
      focusElevation: 0,
      highlightElevation: 3,
      shape: RoundedRectangleBorder(
          borderRadius: borderRadius ?? BorderRadius.circular(15)),
      onPressed: onpressed,
      child: FittedBox(
        fit: isScaledown ? BoxFit.scaleDown : BoxFit.contain,
        child: child ??
            Text(
              text,
              style: TextStyle(fontSize: 20),
            ),
      ),
      textColor: Colors.white,
    ),
  );
}

SizedBox buildheadings({
  @required String text,
  double height,
  double width,
  AlignmentGeometry alignment = Alignment.centerLeft,
  TextAlign textAlign = TextAlign.start,
  int maxLines,
  TextStyle style,
}) {
  return SizedBox(
    height: height,
    width: width,
    child: Align(
      alignment: alignment,
      child: FittedBox(
        child: Text(
          text,
          textAlign: textAlign,
          maxLines: maxLines,
          overflow: TextOverflow.ellipsis,
          style: style,
        ),
      ),
    ),
  );
}

SizedBox buildCaptions({
  @required String text,
  double height,
  double width,
  AlignmentGeometry alignment = Alignment.centerLeft,
  TextAlign textAlign = TextAlign.start,
  TextStyle textStyle,
}) {
  return SizedBox(
    height: 20,
    width: 10,
    child: Align(
      alignment: alignment,
      child: Text(
        text,
        textAlign: textAlign,
        style: textStyle ?? Get.textTheme.subtitle2,
      ),
    ),
  );
}

List<String> intrestCategories = [
  'Cricket',
  'Football',
  'Basketball',
  'Volleyball',
  'Table tennis',
  'Tennis',
  'Badminton',
  'Chess',
  'Dodgeball',
  'Hockey',
];

PreferredSizeWidget buildAppBar(
    {Widget title, Widget leading, List<Widget> actions}) {
  return AppBar(
    leading: leading,
    title: title,
    actions: actions,
    backgroundColor: cWhite,
    // toolbarHeight: 65,
    centerTitle: true,
    elevation: 0,
    iconTheme: Get.theme.iconTheme,
  );
}

Widget buildTextFildErrorBox({
  String errorMsg,
}) {
  if (errorMsg != 'review' && errorMsg != 'success') {
    return Container(
      constraints: BoxConstraints(
        maxHeight: 40,
        minHeight: 20,
        maxWidth: Get.width / 1.2,
      ),
      decoration: const BoxDecoration(
        // color: clightGrey,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset('assets/lottie/errorField.json',
              height: 16, width: 16, fit: BoxFit.cover),
          SizedBox(
            width: Get.width * 0.01,
          ),
          Expanded(
            child: Text(
              '$errorMsg',
              style:
              Get.textTheme.subtitle2.copyWith(color: Get.theme.errorColor),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  } else {
    return const SizedBox();
  }
}

Container buildRoundedBox({Widget child}) {
  return Container(
      height: 80,
      width: 80,
      decoration: BoxDecoration(
          color: cPrimary.withOpacity(0.2), shape: BoxShape.circle),
      child: child);
}

void showMyErrorSnakBar({@required String title, @required String caption}) {
  Get.snackbar(title, caption,
      backgroundColor: Colors.yellow,
      colorText: Colors.black,
      snackPosition: SnackPosition.BOTTOM,
      borderRadius: 5);
}

void botLoading({bool crossPage = true}) {
  BotToast.showCustomLoading(
    toastBuilder: (cancelFunc) => Container(
      decoration: BoxDecoration(
          color: Colors.yellow[200].withOpacity(0.8),
          borderRadius: BorderRadius.circular(10)),
      height: 100,
      width: 100,
      child: const CupertinoActivityIndicator(
        radius: 20,
      ),
    ),
    align: Alignment.center,
    allowClick: false,
    clickClose: false,
    crossPage: crossPage,
  );
}

void botSuccessMsg({String sucessMsg, bool isSuccess}) {
  BotToast.showCustomLoading(
    toastBuilder: (cancelFunc) => Container(
        decoration: BoxDecoration(
          color: Colors.grey[200].withOpacity(0.9),
          borderRadius: BorderRadius.circular(10),
        ),
        height: Get.width <= 350 ? 250 : 300,
        width: Get.width <= 350 ? 250 : 300,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Lottie.asset(
                "assets/lottie/${isSuccess ? "successCheckBox" : "error"}.json",
                height: 90,
                width: 90,
                repeat: false),
            Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    sucessMsg,
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: Colors.blue, fontSize: 18),
                  ),
                ))
          ],
        )),
    align: Alignment.center,
    allowClick: false,
    clickClose: false,
    crossPage: true,
  );
}





Widget buildErrorIcon({Function onPressed, String errorMsg}) {
  if (errorMsg != 'review' && errorMsg != 'success') {
    return SizedBox(
      child: Row(
        children: [
          const SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: onPressed,
            child: Lottie.asset('assets/lottie/errorField.json',
                height: 22, width: 22, fit: BoxFit.cover),
          ),
        ],
      ),
    );
  } else {
    return const SizedBox();
  }
}

List<String> randomImages = [
  'https://i.picsum.photos/id/1072/3872/2592.jpg?hmac=I5d8vixhn6Ne9Ao1YQdtHfxS2YKNyx6_Bu8N_V1-ovk',
  'https://i.picsum.photos/id/1071/3000/1996.jpg?hmac=rPo94Qr1Ffb657k6R7c9Zmfgs4wc4c1mNFz7ND23KnQ',
  'https://i.picsum.photos/id/0/5616/3744.jpg?hmac=3GAAioiQziMGEtLbfrdbcoenXoWAW-zlyEAMkfEdBzQ',
  'https://i.picsum.photos/id/1003/1181/1772.jpg?hmac=oN9fHMXiqe9Zq2RM6XT-RVZkojgPnECWwyEF1RvvTZk',
  'https://i.picsum.photos/id/1015/6000/4000.jpg?hmac=aHjb0fRa1t14DTIEBcoC12c5rAXOSwnVlaA5ujxPQ0I',
  'https://i.picsum.photos/id/1025/4951/3301.jpg?hmac=_aGh5AtoOChip_iaMo8ZvvytfEojcgqbCH7dzaz-H8Y',
  'https://i.picsum.photos/id/1024/1920/1280.jpg?hmac=-PIpG7j_fRwN8Qtfnsc3M8-kC3yb0XYOBfVzlPSuVII',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/1070/5472/3648.jpg?hmac=oFxAwLeGJmas45_yf5NdpeQzexAF-tMVL6q9JwvSuo0',
  'https://i.picsum.photos/id/1015/6000/4000.jpg?hmac=aHjb0fRa1t14DTIEBcoC12c5rAXOSwnVlaA5ujxPQ0I',
  'https://i.picsum.photos/id/1025/4951/3301.jpg?hmac=_aGh5AtoOChip_iaMo8ZvvytfEojcgqbCH7dzaz-H8Y',
  'https://i.picsum.photos/id/1024/1920/1280.jpg?hmac=-PIpG7j_fRwN8Qtfnsc3M8-kC3yb0XYOBfVzlPSuVII',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/1070/5472/3648.jpg?hmac=oFxAwLeGJmas45_yf5NdpeQzexAF-tMVL6q9JwvSuo0',
  'https://i.picsum.photos/id/1015/6000/4000.jpg?hmac=aHjb0fRa1t14DTIEBcoC12c5rAXOSwnVlaA5ujxPQ0I',
  'https://i.picsum.photos/id/1025/4951/3301.jpg?hmac=_aGh5AtoOChip_iaMo8ZvvytfEojcgqbCH7dzaz-H8Y',
  'https://i.picsum.photos/id/1024/1920/1280.jpg?hmac=-PIpG7j_fRwN8Qtfnsc3M8-kC3yb0XYOBfVzlPSuVII',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/106/2592/1728.jpg?hmac=E1-3Hac5ffuCVwYwexdHImxbMFRsv83exZ2EhlYxkgY',
  'https://i.picsum.photos/id/1070/5472/3648.jpg?hmac=oFxAwLeGJmas45_yf5NdpeQzexAF-tMVL6q9JwvSuo0',
];

List<String> monthsOfAYear = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];
