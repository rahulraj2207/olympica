import 'package:flutter/material.dart';

class AssetsUtils {
  //Profile

  static String blockUser = 'assets/svg/profile/block_user.svg';
  static String deactivateAccount = 'assets/svg/profile/deactivate_account.svg';
  static String iniviteUser = 'assets/svg/profile/invite_user.svg';
  static String logout = 'assets/svg/profile/logout.svg';
  static String passwordReset = 'assets/svg/profile/password_reset.svg';
  static String personalInfo = 'assets/svg/profile/personal_info.svg';
  static String privacyPolice = 'assets/svg/profile/privacy_police.svg';
  static String req_verification = 'assets/svg/profile/req_verificatoion.svg';
  static String settings = 'assets/svg/profile/settings.svg';
  static String termsConditions = 'assets/svg/profile/t&c.svg';
  static String userGuid = 'assets/svg/profile/user_guid.svg';

  //global
  static String share = 'assets/svg/share.svg';
  static String calendar = 'assets/svg/calender.svg';
  static String restricted = 'assets/svg/restricted.svg';
}
