import 'package:flutter/material.dart';

Color orange = Color(0xffFF9E40);
Color blue = Color(0xff41287B);

List<String> months = [
  'JAN',
  'FEB',
  'MAR',
  'APR',
  'MAY',
  'JUN',
  'JUL',
  'AUG',
  'SEP',
  'OCT',
  'NOV',
  'DEC'
];

double height(BuildContext context) => MediaQuery.of(context).size.height;
double width(BuildContext context) => MediaQuery.of(context).size.width;
Map<String, Widget> facilities = <String, Widget>{
  'WASHROOM': Image(image: AssetImage('assets/images/fac1.png')),
  'CHANGINGROOM': Image(image: AssetImage('assets/images/fac2.png')),
  'DRINKINGWATER': Image(image: AssetImage('assets/images/fac3.png')),
  'PARKING': Image(image: AssetImage('assets/images/fac4.png')),
  'FIRSTAID': Image(image: AssetImage('assets/images/fac5.png')),
};
