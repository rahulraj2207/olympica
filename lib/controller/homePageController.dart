import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/widgets/bottomNavBar.dart';
import 'package:sportsgram/controller/UserDataController.dart';

class HomePageController extends GetxController {

  DateTime selectedDateTime;

  void changeSelectedDate(DateTime dateTime) {
    selectedDateTime = dateTime;
    update();
  }
  final pageController = PageController();
  final eventsScrollController = ScrollController();
  final searchScrollController = ScrollController();
  final userData = Get.find<UserDataController>();

  //!Bottom navy bar
  BottomNavSelected selectedBottomNav = BottomNavSelected.feed;
  bool hideBottomWidgets = false;
  bool flag = false;

  void selectBottomNar({@required BottomNavSelected val, @required int navTo}) {
    print('object');
    selectedBottomNav = val;
    pageController.jumpToPage(
      navTo,
    );
    update();
  }

  @override
  void onInit() {
    super.onInit();
    scrollController();
  }

  void scrollController() {
    //events
    eventsScrollController.addListener(() {
      if (eventsScrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        hideBottomWidgets = true;
      } else {
        hideBottomWidgets = false;
      }
      //for avoiding continues update
      if (hideBottomWidgets != flag) {
        update();
        print('u[dated');
      }
      flag = hideBottomWidgets;
    });

    //search
    searchScrollController.addListener(() {
      if (searchScrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        hideBottomWidgets = true;
      } else {
        hideBottomWidgets = false;
      }
      //for avoiding continues update
      if (hideBottomWidgets != flag) {
        update();
        print('u[dated');
      }
      flag = hideBottomWidgets;
    });
  }


}
