import 'package:firebase_auth/firebase_auth.dart';
import 'package:sportsgram/owner/ownerAccount.dart';
import 'package:sportsgram/owner/ownerPage.dart';
import 'package:sportsgram/screens/Auth/Screens/allMatches.dart';
import 'package:sportsgram/screens/Auth/Screens/matches.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/yourMatches.dart';



class tabController extends GetxController{
  int tabIndex = 0;
  PageController mainPageCtr;
  @override
  void onInit() {
    super.onInit();
  }


  void onTabChaged(int index) {
    tabIndex = index;
    update();
  }
}




String userEmail;

class PageBuilder extends StatefulWidget {

  final double height;
  final double width;
  const PageBuilder({Key key, this.height, this.width}) : super(key: key);

  @override
  _PageBuilderState createState() => _PageBuilderState();
}

class _PageBuilderState extends State<PageBuilder> {



  final _auth = FirebaseAuth.instance;

  User loggedInUser;

  void getCurrentUseronMatches()async{
    try{
      final user = await _auth.currentUser;
      if(user!= null){
        loggedInUser = user;
        String el = loggedInUser.email;
        List uE = el.split("@");
        userEmail= uE[0];
      }
    }catch(e){
      print(e);
    }

  }


  final controller = Get.find<tabController>();

  void initState() {
    super.initState();
    getCurrentUseronMatches();
    print(userEmail);
    controller.mainPageCtr = PageController();
  }




  final List<Widget> pageWidgets = [

    AllMatches(),
    YourMatches(email: userEmail),

  ];


  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      controller: controller.mainPageCtr,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 2,
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return pageWidgets[index];
      },
    );
  }
}
