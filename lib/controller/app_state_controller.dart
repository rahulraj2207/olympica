import 'package:get/get.dart';

class AppStateController extends GetxController {
  bool appLoader = false;
  bool currentStateLoader = false;

  void currentStateLoading(bool val) {
    currentStateLoader = val;
    update();
  }

  void appLoadState(bool val) {
    appLoader = val;
    update();
  }

}
