import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SignUpData{
  String name;
  String surname;
  String username;
  String email1;
  String key;
  String bio;



  SignUpData({this.name,this.surname,this.username,this.email1,this.key,this.bio});
  Map<String,dynamic>tomap(){
    return{'UNAME':name,'SURNAME':surname,'USERNAME':username,'EMAIL':email1,'KEY':key,'BIO':bio};
  }

}


class SignUpStore{




  DatabaseReference database1(String email){
    return FirebaseDatabase.instance.reference().child('UserSignUp').child(email);
  }

  void setToDatabase1(SignUpData v,String email)async{
    await database1(email).push().set(v.tomap());

    database1(email).onChildAdded.listen((Event event) {
      print('${event.snapshot.key}');

      database1(email).child(event.snapshot.key).update({'KEY': event.snapshot.key}).then((_) {
        print('Transaction  committed.');
      });



    });



  }

  Future<List<SignUpData>>getFromDb1(String emaillll)async{
    var data=await database1(emaillll).once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<SignUpData>lst=List<SignUpData>();
      mp.forEach((key, value) {

        lst.add(SignUpData(name: value['UNAME'],surname: value['SURNAME'],username: value['USERNAME'],email1: value['EMAIL'],key: value['KEY'],bio: value['BIO']));
      });
      return lst;
    }
  }
List userList;

  void update(String em,String bio,String key)async{

    await database1(em).child(key).update({'BIO': bio}).then((_) {
      print('Transaction  committed.');
    });

  }





  }










