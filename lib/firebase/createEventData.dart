import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class Match{
  String key;
  String mname;
  String hname;
  int mnum;
  String mground;
  String mdate;
  String mtime;
  String mphone;
  String mcategory;
  String image;
  bool join;


  Match({this.join,this.key,this.mname,this.hname,this.mnum,this.mground,this.mdate,this.mtime,this.mphone,this.mcategory,this.image});
  Map<String,dynamic>tomap(){
    return{'MIMAGE': image,'MCATEGORY': mcategory,'MNAME':mname,'MHOSTNAME':hname,'MPLAYERS':mnum,'MGROUND':mground,'MDATE':mdate,'MTIME':mtime,'MPHONE':mphone,'JOIN':join,'KEY':key};
  }
}
class MatchStore{
  DatabaseReference database(){
    return FirebaseDatabase.instance.reference().child('ALLMATCHES');
  }
  void setToDatabase(Match v)async{

    await database().push().set(v.tomap());

    database().onChildAdded.listen((Event event) {
      print('${event.snapshot.key}');
      database().child(event.snapshot.key).update({'KEY': event.snapshot.key}).then((_) {
        print('Transaction  committed.');
      });

    });
  }

  Future<List<Match>>getFromDb()async{
    var data=await database().once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<Match>lst=List<Match>();
      mp.forEach((key, value) {
        lst.add(Match(image: value['MIMAGE'] ,mname: value['MNAME'],hname: value['MHOSTNAME'],mnum: value['MPLAYERS'],mground: value['MGROUND'],mdate: value['MDATE'],mtime: value['MTIME'],mphone: value['MPHONE'],mcategory: value['MCATEGORY'],join: value['JOIN'],key: value['KEY']));
      });
      return lst;
    }
  }




  DatabaseReference databaseInuser(String s){
    return FirebaseDatabase.instance.reference().child('USERSMATCHES').child(s);
  }

  void setToDatabaseInuser(Match v,String m)async{
    await databaseInuser(m).push().set(v.tomap());

   // await databaseInuser(m).onChildAdded.listen((Event event) {
   //    print('${event.snapshot.key}');
   //    databaseInuser(m).update({'KEY': event.snapshot.key}).then((_) {
   //      print('Transaction  committed.');
   //    });
   //
   //  });


  }



  Future<List<Match>>getFromDBInuser(String ema)async{
    var data=await databaseInuser(ema).once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<Match>lstU=List<Match>();
      mp.forEach((key, value) {
        lstU.add(Match(image: value['MIMAGE'] ,mname: value['MNAME'],hname: value['MHOSTNAME'],mnum: value['MPLAYERS'],mground: value['MGROUND'],mdate: value['MDATE'],mtime: value['MTIME'],mphone: value['MPHONE'],mcategory: value['MCATEGORY'],join: value['JOIN'],key: value['KEY']));
      });
      return lstU;
    }
  }








  void updateJoin(String key)async{
    print("joinkey");
    print(key);
    await database().child(key).update({'JOIN': true}).then((_) {
      print('updated value  committed.');
    });

  }


void deletedata(String key)async{
  await database().child(key).remove();
}
}


