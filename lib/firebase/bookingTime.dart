import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class Time{
String time;


  Time({this.time});
  Map<String,dynamic>tomap(){
    return{'TIME': time};
  }
}



class TimeStore{
  DatabaseReference databaseT(){
    return FirebaseDatabase.instance.reference().child('BOOKINGTIME');
  }


  void setToDatabaseT(Time v)async{
    await databaseT().push().set(v.tomap());
  }
// String t = "";
  Future<List<Time>>getFromDbT()async{
    var data=await databaseT().once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<Time>lst=List<Time>();
      mp.forEach((key, value) {
        lst.add(Time(time: value['TIME']));
      });
      return lst;
    }
  }




  void getTime(String tym)async{

    for(var T in await TimeStore().getFromDbT()) {

      if (T.time == tym) {
        print(T.time);
        BotToast.showText(
            text: "Already Booked",
            align: Alignment.center
        );
        print("no");
      }
    }
  }

}


