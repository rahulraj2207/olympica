// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_database/firebase_database.dart';
// import 'package:flutter/cupertino.dart';
//
// class Ground{
//   String gname;
//   String gdetails;
//   String gLOc;
//   String gabout;
//   String gimage;
//   double lat;
//   double long;
//   String open;
//
//   Ground({this.gname,this.gdetails,this.gLOc,this.gabout,this.gimage,this.lat,this.long,this.open});
//   Map<String,dynamic>tomap(){
//     return{'GIMAGE':gimage,'GNAME':gname,'GDETAILS':gdetails,'GLOC':gLOc,'GABOUT':gabout,'LAT':lat,'LONG':long,'OPEN':open};
//   }
// }
// class GroundStore{
//
//
//
//   DatabaseReference databaseU(){
//     return FirebaseDatabase.instance.reference().child('GroundDataUser');
//   }
//   void setToDatabaseU(Ground v)async{
//     await databaseU().push().set(v.tomap());
//   }
//   Future<List<Ground>>getFromDbU()async{
//     var data=await databaseU().once();
//     Map<dynamic,dynamic>mp=data.value;
//     if(mp!=null){
//       List<Ground>lstU=List<Ground>();
//       mp.forEach((key, value) {
//         lstU.add(Ground(gimage: value['GIMAGE'],gname: value['GNAME'],gdetails: value['GDETAILS'],gLOc: value['GLOC'],
//             gabout: value['GABOUT'],lat: value['LAT'],long: value['LONG'],open: value['OPEN']));
//       });
//       return lstU;
//     }
//   }
//
//
//
//   DatabaseReference databaseO(String s){
//     return FirebaseDatabase.instance.reference().child('GroundDataOwner').child(s);
//   }
//   void setToDatabaseO(Ground v,String m)async{
//     await databaseO(m).push().set(v.tomap());
//   }
//
//
//   Future<List<Ground>>getFromDBO(String ema)async{
//     var data=await databaseO(ema).once();
//     Map<dynamic,dynamic>mp=data.value;
//     if(mp!=null){
//       List<Ground>lstO=List<Ground>();
//       mp.forEach((key, value) {
//
//         lstO.add(Ground(gimage: value['GIMAGE'],gname: value['GNAME'],gdetails: value['GDETAILS'],gLOc: value['GLOC'],
//             gabout: value['GABOUT'],lat: value['LAT'],long: value['LONG'],open: value['OPEN']));
//
//       });
//       return lstO;
//     }
//   }
//
//
//
//
// var snapShotKeyToDel;
//
//   void deletedata()async{
//     FirebaseDatabase.instance.reference()
//         .child("Top").limitToLast(1).once()
//         .then((DataSnapshot snapshot) {
//       Map map = snapshot.value;
//       snapShotKeyToDel = map.keys.toList()[0].toString();
//     });
//
//     // getkey();
//     await FirebaseDatabase.instance.reference()
//         .child("Top").child(snapShotKeyToDel).remove();
//   }
//
//
// }
//
//
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class Ground{
  int id;
  String gname;
  String gdetails;
  String gLOc;
  String gabout;
  int grating;
  String gimage;
  double lat;
  double long;
  String open;
  String close;

  Ground({this.id,this.gname,this.gdetails,this.gLOc,this.gabout,this.grating,this.gimage,this.lat,this.long,this.open,this.close});
  Map<String,dynamic>tomap(){
    return{'GIMAGE':gimage,'GNAME':gname,'GDETAILS':gdetails,'GLOC':gLOc,'GABOUT':gabout,"GRATING":grating,'LAT':lat,'LONG':long,'OPEN':open,'CLOSE':close};
  }
}
class GroundStore{



  DatabaseReference databaseU(){
    return FirebaseDatabase.instance.reference().child('GroundDataUser');
  }
  void setToDatabaseU(Ground v)async{
    await databaseU().push().set(v.tomap());
  }



  DatabaseReference databaseO(String s){
    return FirebaseDatabase.instance.reference().child('GroundDataOwner').child(s);
  }
  void setToDatabaseO(Ground v,String m)async{
    await databaseO(m).push().set(v.tomap());
  }


  Future<List<Ground>>getFromDbU()async{
    var data=await databaseU().once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<Ground>lstU=List<Ground>();
      mp.forEach((key, value) {
        lstU.add(Ground(gimage: value['GIMAGE'],gname: value['GNAME'],gdetails: value['GDETAILS'],gLOc: value['GLOC'],
            gabout: value['GABOUT'],grating: value['GRATING'],lat: value['LAT'],long: value['LONG'],open: value['OPEN'],close: value['CLOSE']));
      });
      return lstU;
    }
  }




  Future<List<Ground>>getFromDBO(String ema)async{
    var data=await databaseO(ema).once();
    Map<dynamic,dynamic>mp=data.value;
    if(mp!=null){
      List<Ground>lstO=List<Ground>();
      mp.forEach((key, value) {

        lstO.add(Ground(gimage: value['GIMAGE'],gname: value['GNAME'],gdetails: value['GDETAILS'],gLOc: value['GLOC'],
            gabout: value['GABOUT'],grating: value['GRATING'],lat: value['LAT'],long: value['LONG'],open: value['OPEN'],close: value['CLOSE']));

      });
      return lstO;
    }
  }






  // void getkey()async{
  //
  //   await FirebaseDatabase.instance.reference()
  //       .child("Top").limitToLast(1).once()
  //       .then((DataSnapshot snapshot) {
  //     Map map = snapshot.value;
  //     snapShotKeyToDel = map.keys.toList()[0].toString();
  //   });
  //
  // }
  var snapShotKeyToDel;

  void deletedata()async{
    FirebaseDatabase.instance.reference()
        .child("Top").limitToLast(1).once()
        .then((DataSnapshot snapshot) {
      Map map = snapshot.value;
      snapShotKeyToDel = map.keys.toList()[0].toString();
    });

    // getkey();
    await FirebaseDatabase.instance.reference()
        .child("Top").child(snapShotKeyToDel).remove();
  }



}


