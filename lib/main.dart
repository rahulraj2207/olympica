import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/UserDataController.dart';
import 'package:sportsgram/controller/app_state_controller.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/ownerController/ownerSignUpController.dart';
import 'package:sportsgram/screens/Auth/Controllers/getMatchListController.dart';
import 'package:sportsgram/screens/Auth/Controllers/profileController.dart';
import 'package:sportsgram/screens/Auth/Screens/splash.dart';
import 'bindings/mainBindings.dart';
import 'package:sportsgram/screens/Auth/Controllers/loginController.dart';
import 'package:sportsgram/ownerController/ownerLoginController.dart';


import 'screens/Auth/Controllers/getGroundListController.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Get.put(UserDataController());
  Get.put(AppStateController());
  Get.put(OwnerLoginController());
  Get.put(OwnerSignUpController());
  Get.put(LoginController());
  Get.put(GetListControllerU());
  Get.put(ProfileController());
  Get.put(HomePageController());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      builder: BotToastInit(),
      navigatorObservers: [BotToastNavigatorObserver()],
      // initialBinding: MainBindings(),
      home:splash(),
    );
  }
}


// class MyHomeWidget extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     print(MediaQuery.of(context).orientation);
//     return GetBuilder<UserDataController> (
//       builder: (controller){
//         print('object');
//         WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
//           if (controller.session != null) {
//             print('hello');
//             Get.offAll(HomeScreen(), transition: Transition.cupertino);
//           } else if (controller.session == null && !controller.isLoading) {
//             Get.offAll(LoginScreen(), transition: Transition.cupertino);
//           }
//         });
//         return Scaffold(
//           body: Center(
//             child: CircularProgressIndicator(
//               backgroundColor: Colors.green,
//             ),
//           ),
//         );
//       },
//     );
//   }
// }


