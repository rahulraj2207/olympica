import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/homePageController.dart';

enum BottomNavSelected {
  search,
  feed,
  profile,
  addEvent,
}

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Container(
        height: 60,
        decoration: BoxDecoration(
            color: Colors.green.shade900,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        width: size.width,
        child: GetBuilder<HomePageController>(
          builder: (controller) => Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [

              IconButton(
                  icon: SvgPicture.asset(
                    'assets/svg/feeds.svg',
                    color:
                    controller.selectedBottomNav == BottomNavSelected.feed
                        ? Colors.yellow
                        : Colors.white,
                    height:
                    controller.selectedBottomNav == BottomNavSelected.feed
                        ? 20
                        : 16,
                  ),
                  onPressed: () {
                    controller.selectBottomNar(
                        val: BottomNavSelected.feed, navTo: 0);
                  },                splashColor: Color(0xffF4F4F9),

              ),

              IconButton(
                icon: SvgPicture.asset('assets/svg/search.svg',
                  color:
                  controller.selectedBottomNav == BottomNavSelected.search
                      ? Colors.yellow
                      : Colors.white,
                  height:
                  controller.selectedBottomNav == BottomNavSelected.search
                      ? 20
                      : 16,
                ),

                onPressed: () {

                  controller.selectBottomNar(
                      val: BottomNavSelected.search, navTo: 1);
                },
              ),

              Align(
                alignment: Alignment.topCenter,
                child: IconButton(
                    icon: Icon(
                      CupertinoIcons.plus_circled,
                      color:
                    controller.selectedBottomNav ==
                            BottomNavSelected.addEvent
                            ? Colors.yellow
                            : Colors.white,
                        size: controller.selectedBottomNav ==
                            BottomNavSelected.addEvent
                            ? 37
                            : 30,
                    ),

                    onPressed: () {
                      controller.selectBottomNar(
                          val: BottomNavSelected.addEvent, navTo: 2);
                    }),
              ),
              // IconButton(
              //     icon: SvgPicture.asset(
              //       'assets/svg/notification.svg',
              //       color: controller.selectedBottomNav ==
              //           BottomNavSelected.notifications
              //           ? Colors.yellow
              //           : Colors.white,
              //       height: controller.selectedBottomNav ==
              //           BottomNavSelected.notifications
              //           ? 20
              //           : 16,
              //     ),
              //     onPressed: () {
              //       controller.selectBottomNar(
              //           val: BottomNavSelected.notifications, navTo: 2);
              //     }),
              IconButton(
                  icon: SvgPicture.asset(
                    'assets/svg/profile.svg',
                    color: controller.selectedBottomNav ==
                        BottomNavSelected.profile
                        ? Colors.yellow
                        : Colors.white,
                    height: controller.selectedBottomNav ==
                        BottomNavSelected.profile
                        ? 20
                        : 16,
                  ),
                  onPressed: () {
                    controller.selectBottomNar(
                        val: BottomNavSelected.profile, navTo: 3);
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
