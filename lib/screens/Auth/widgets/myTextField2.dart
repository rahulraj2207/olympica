import
'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/ownerController/ownerLoginController.dart';
import 'package:sportsgram/ownerController/ownerSignUpController.dart';
import 'package:sportsgram/utils/utils/Icons/custom_icons_icons.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';

enum TextFieldSwitchers1 {
  sFirstName,
  sSurname,
  sEmail,
  sUserName,
  sPassword,
  lEmail,
  lPassword,
}

class MyTextFiled1 extends StatefulWidget {
  final TextFieldSwitchers1 enums;
  final String hintText;
  MyTextFiled1({
    @required this.enums,
    @required this.hintText,
  });

  @override
  _MyTextFiled1State createState() => _MyTextFiled1State();
}

class _MyTextFiled1State extends State<MyTextFiled1> {

  String nname;
  String ppassword;

  final FocusNode _commonFocus = FocusNode();
  TextEditingController ctr;
  bool hasFocus = false;
  bool passwordEye = true;

  @override
  void initState() {
    super.initState();
    ctr = TextEditingController();
    _commonFocus.addListener(() {
      if (_commonFocus.hasFocus) {
        setState(() {
          hasFocus = true;
        });
      } else {
        setState(() {
          hasFocus = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _commonFocus.dispose();
    ctr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildBgInputBox(
      child: TextField(
        controller: ctr,
        focusNode: _commonFocus,
        onChanged: _onChanged,
        decoration: InputDecoration(
            contentPadding: contentPading(),
            prefixIcon: preFixIcon(),
            border: InputBorder.none,
            hintText: widget.hintText,
            suffixIcon: sufixIcon()
        ),
        obscureText: obsecureTxt(),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  void _onChanged(String val) {


    switch (widget.enums) {
      case TextFieldSwitchers1.sFirstName:
        {

          Get.find<OwnerSignUpController>().validateName(val);
          print('firestName');
        }
        break;
      case TextFieldSwitchers1.sSurname:
        {
          print('SSurname');
          Get.find<OwnerSignUpController>().validateSurname(val);
        }
        break;
      case TextFieldSwitchers1.sEmail:
        {
          print('email');
          Get.find<OwnerSignUpController>().validateEmail(val);
        }
        break;
      case TextFieldSwitchers1.sUserName:
        {
          print('us');
          Get.find<OwnerSignUpController>().validateUserName(val);
        }
        break;
      case TextFieldSwitchers1.sPassword:
        {
          print('pss');
          Get.find<OwnerSignUpController>().validatePassword(val);
        }
        break;
      case TextFieldSwitchers1.lEmail:
        {
          print('LogE');
          Get.find<OwnerLoginController>().validateEmailUserMob(val);
        }
        break;
      case TextFieldSwitchers1.lPassword:
        {
          print('LogP');
          Get.find<OwnerLoginController>().validatePassword(val);
        }
        break;
      default:
        {
          print('defaukl');
        }
    }
  }

  bool obsecureTxt() {
    return widget.enums == TextFieldSwitchers1.sPassword
        ? true
        : widget.enums == TextFieldSwitchers1.lPassword
        ? passwordEye
        : false;
  }

  Widget preFixIcon() {
    return (widget.enums == TextFieldSwitchers1.lEmail ||
        widget.enums == TextFieldSwitchers1.lPassword)
        ? Icon(
      widget.enums == TextFieldSwitchers1.lEmail
          ? CupertinoIcons.person_circle_fill
          : Icons.lock,
      color: hasFocus ? Colors.green : cDarkGrey,
    )
        : null;
  }

  EdgeInsetsGeometry contentPading() {
    return (widget.enums != TextFieldSwitchers1.lEmail &&
        widget.enums != TextFieldSwitchers1.lPassword)
        ? const EdgeInsets.symmetric(horizontal: 10)
        : null;
  }

  Widget sufixIcon() {
    if ((widget.enums == TextFieldSwitchers1.lPassword)) {
      return GetX<OwnerLoginController>(
        builder: (controller) => controller.passwordError.value != 'success'
            ? const IconButton(icon: Icon(CustomIcons.eye_off), onPressed: null)
            : IconButton(
          icon: Icon(
            !passwordEye ? CustomIcons.eye : CustomIcons.eye_off,
            color: hasFocus ? Colors.green : cDarkGrey,
          ),
          onPressed: () {
            setState(() {
              passwordEye = !passwordEye;
            });
          },
        ),
      );
    } else if (widget.enums == TextFieldSwitchers1.lEmail) {
      return IconButton(
        icon: const Icon(Icons.contact_support_rounded),
        onPressed: () {
          BotToast.showText(
              text: 'Enter your Email address',
              align: const Alignment(0, 0));
        },
      );
    } else {
      return null;
    }
  }
}
