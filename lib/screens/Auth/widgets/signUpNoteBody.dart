import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/signUpScreen.dart';
class SignUpNoteBody extends StatefulWidget {
  const SignUpNoteBody({
    Key key,
  }) : super(key: key);

  @override
  _SignUpNoteBodyState createState() => _SignUpNoteBodyState();
}

class _SignUpNoteBodyState extends State<SignUpNoteBody> {
  TapGestureRecognizer onTapGD;

  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      print("completed");
      setState(() {});
    });


    super.initState();
    onTapGD = TapGestureRecognizer()..onTap = _signUp;
  }

  @override
  void dispose() {
    onTapGD.dispose();
    super.dispose();
  }

  void _signUp() {
    Get.focusScope.unfocus();
    Get.to(
      SignUpScreen(),
      transition: Transition.downToUp,
    );
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Not on Olympica yet? ',
          style: TextStyle(fontSize: 15,color:  Colors.yellow.shade600),
          children: [
            TextSpan(
              recognizer: onTapGD,
              text: 'Sign up',
              style: TextStyle(fontSize: 18,fontWeight: FontWeight.w700,color: Colors.yellow.shade700),
            )
          ]),
    );
  }
}
