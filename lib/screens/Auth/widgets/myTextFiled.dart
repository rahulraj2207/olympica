import
'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Controllers/loginController.dart';
import 'package:sportsgram/screens/Auth/Controllers/signUpController.dart';
import 'package:sportsgram/utils/utils/Icons/custom_icons_icons.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';

enum TextFieldSwitchers {
  sFirstName,
  sSurname,
  sEmail,
  sUserName,
  sPassword,
  lEmail,
  lPassword,
}

class MyTextFiled extends StatefulWidget {
  final TextFieldSwitchers enums;
  final String hintText;
  MyTextFiled({
    @required this.enums,
    @required this.hintText,
  });

  @override
  _MyTextFiledState createState() => _MyTextFiledState();
}

class _MyTextFiledState extends State<MyTextFiled> {

  String nname;
  String ppassword;

  final FocusNode _commonFocus = FocusNode();
  TextEditingController ctr;
  bool hasFocus = false;
  bool passwordEye = true;

  @override
  void initState() {
    super.initState();
    ctr = TextEditingController();
    _commonFocus.addListener(() {
      if (_commonFocus.hasFocus) {
        setState(() {
          hasFocus = true;
        });
      } else {
        setState(() {
          hasFocus = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _commonFocus.dispose();
    ctr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildBgInputBox(
      child: TextField(
        controller: ctr,
        focusNode: _commonFocus,
        onChanged: _onChanged,
        decoration: InputDecoration(
            contentPadding: contentPading(),
            prefixIcon: preFixIcon(),
            border: InputBorder.none,
            hintText: widget.hintText,
            suffixIcon: sufixIcon()
         ),
        obscureText: obsecureTxt(),
        textInputAction: TextInputAction.next,
      ),
    );
  }

  void _onChanged(String val) {


    switch (widget.enums) {
      case TextFieldSwitchers.sFirstName:
        {

          Get.find<SignUpController>().validateName(val);
          print('firestName');
        }
        break;
      case TextFieldSwitchers.sSurname:
        {
          print('SSurname');
          Get.find<SignUpController>().validateSurname(val);
        }
        break;
      case TextFieldSwitchers.sEmail:
        {
          print('email');
          Get.find<SignUpController>().validateEmail(val);
        }
        break;
      case TextFieldSwitchers.sUserName:
        {
          print('us');
          Get.find<SignUpController>().validateUserName(val);
        }
        break;
      case TextFieldSwitchers.sPassword:
        {
          print('pss');
          Get.find<SignUpController>().validatePassword(val);
        }
        break;
      case TextFieldSwitchers.lEmail:
        {
          print('LogE');
          Get.find<LoginController>().validateEmailUserMob(val);
        }
        break;
      case TextFieldSwitchers.lPassword:
        {
          print('LogP');
          Get.find<LoginController>().validatePassword(val);
        }
        break;
      default:
        {
          print('defaukl');
        }
    }
  }

  bool obsecureTxt() {
    return widget.enums == TextFieldSwitchers.sPassword
        ? true
        : widget.enums == TextFieldSwitchers.lPassword
        ? passwordEye
        : false;
  }

  Widget preFixIcon() {
    return (widget.enums == TextFieldSwitchers.lEmail ||
        widget.enums == TextFieldSwitchers.lPassword)
        ? Icon(
      widget.enums == TextFieldSwitchers.lEmail
          ? CupertinoIcons.person_circle_fill
          : Icons.lock,
      color: hasFocus ? Colors.green : cDarkGrey,
    )
        : null;
  }

  EdgeInsetsGeometry contentPading() {
    return (widget.enums != TextFieldSwitchers.lEmail &&
        widget.enums != TextFieldSwitchers.lPassword)
        ? const EdgeInsets.symmetric(horizontal: 10)
        : null;
  }

  Widget sufixIcon() {
    if ((widget.enums == TextFieldSwitchers.lPassword)) {
      return GetX<LoginController>(
        builder: (controller) => controller.passwordError.value != 'success'
            ? const IconButton(icon: Icon(CustomIcons.eye_off), onPressed: null)
            : IconButton(
          icon: Icon(
            !passwordEye ? CustomIcons.eye : CustomIcons.eye_off,
            color: hasFocus ? Colors.green : cDarkGrey,
          ),
          onPressed: () {
            setState(() {
              passwordEye = !passwordEye;
            });
          },
        ),
      );
    } else if (widget.enums == TextFieldSwitchers.lEmail) {
      return IconButton(
        icon: const Icon(Icons.contact_support_rounded),
        onPressed: () {
          BotToast.showText(
              text: 'Enter your Email address',
              align: const Alignment(0, 0));
        },
      );
    } else {
      return null;
    }
  }
}
