import 'package:flutter/material.dart';

class RatingWithStar extends StatelessWidget {
  final int rating;
  final MainAxisAlignment mainAxisAlignment;
  const RatingWithStar({Key key, @required this.rating, this.mainAxisAlignment}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment:mainAxisAlignment?? MainAxisAlignment.end,
      children: [
        for (var i = 0; i < 5; i++)
          Icon(
            Icons.star,
            color: rating >= i + 1 ? Colors.red : Colors.grey,
            size: 18,
          )
      ],
    );
  }
}
