import 'package:flutter/material.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
class SearchTextField extends StatelessWidget {
  final void Function(String onChanged) onChangedDataBack;
  final TextEditingController textCtr;

  const SearchTextField({Key key, @required this.onChangedDataBack, this.textCtr})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return buildBgInputBox(
        height: 55,
        child: TextField(
          controller: textCtr,
          decoration: const InputDecoration(
            hintText: 'Search',
            border: InputBorder.none,
            prefixIcon: Icon(Icons.search),
          ),
          onChanged: onChangedDataBack,
        ));
  }
}
