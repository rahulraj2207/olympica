// import 'dart:convert';
//
import 'package:bot_toast/bot_toast.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/HomeScreen.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

 class LoginController extends GetxController {

  String emailUserMob;
  String password;

  final _auth = FirebaseAuth.instance;

  var emailUserMobError = 'review'.obs;
  var passwordError = 'review'.obs;

  void validateEmailUserMob(String val) {
    emailUserMob = val;
    update();
    if (val == null) {
      print("email");
      emailUserMobError.value = 'Enter your Email ';
    } else if (val.trim().isEmpty) {
      print("email1");

      emailUserMobError.value = 'Enter your Email';
    } else {
      emailUserMobError.value = 'success';
    }
  }

  void validatePassword(String val) {
    password = val;
    update();
    if (val == null) {
      print("pwrd");

      passwordError.value = 'Enter your Password';
    } else if (val.trim().isEmpty) {
      print("pwrd1");

      passwordError.value = 'Enter your Password';
    } else {
      passwordError.value = 'success';
    }
  }



  void validateRahul()async{


    validateEmailUserMob(emailUserMob);
    validatePassword(password);




    if (emailUserMobError.value != 'success') {
      showMyErrorSnakBar(
          title: emailUserMobError.value, caption: 'Login Failed');
    } else if (passwordError.value != 'success') {
      showMyErrorSnakBar(title: passwordError.value, caption: 'Login Failed');
    }else{
      botLoading();
      try{
        final user = await _auth.signInWithEmailAndPassword(email: emailUserMob, password: password);
        if(user!=null){

          loginSuccessToast("Login successfull");

          var prefs = await SharedPreferences.getInstance();
          prefs.setBool('islogged', true);
            prefs.setString('email', emailUserMob);

          Get.offAll(() => HomeScreen(),transition: Transition.cupertino);
              BotToast.closeAllLoading();
        }else{
          print("elseinif");
        }
      }catch(e){
        print(emailUserMobError);
        print(passwordError);
        print(emailUserMob);
        print(password);
        BotToast.closeAllLoading();
        loginFailedToast("Login Failed");
        print("why");
        print(e);
      }
    }
  }




  Future<void> loginFailedToast(String error) {
    botSuccessMsg(sucessMsg: error, isSuccess: false);
    return Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }

  void loginSuccessToast(String msg) {
    botSuccessMsg(sucessMsg: msg, isSuccess: true);
    Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }
 }
