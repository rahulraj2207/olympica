import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/HomeScreen.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:sportsgram/firebase/signUpData.dart';


class SignUpController extends GetxController {






  final scrollController = ScrollController();




  final _auth = FirebaseAuth.instance;

  String name;
  String surname;
  String username;
  String email;
  String password;


  // SignUpController({this.name,this.surname,this.username,this.email});
  //
  // Future<List<SignUpController>>getFromDb()async{
  //   var data=await database().once();
  //   Map<dynamic,dynamic>mp=data.value;
  //   if(mp!=null){
  //     List<SignUpController>lst=List<SignUpController>();
  //     mp.forEach((key, value) {
  //       lst.add(SignUpController(name: value['UNAME'],surname: value['SURNAME'],username: value['USERNAME'],email: value['EMAIL']));
  //     });
  //     return lst;
  //   }
  // }



  final dateCtr = TextEditingController();

  //!SIgnUp Validations
  var nameError = 'review'.obs;
  var surnameError = 'review'.obs;
  var userNameError = 'review'.obs;
  var emailError = 'review'.obs;
  var dobError = 'review'.obs;
  var passwordError = 'review'.obs;
  var phoneError = 'review'.obs;
  var tcError = 'review'.obs;



  void validateName(String val) {
    name = val;
    update();
    if (val == null) {
      nameError.value = 'Enter your name';
    } else if (val.trim().isEmpty) {
      nameError.value = 'Enter your name';
    } else if (val.trim().length > 30) {
      nameError.value = 'Enter valid name';
    } else {
      nameError.value = 'success';
    }
  }




  void validateSurname(String val) {
    surname = val;
    update();
    if (val == null) {
      surnameError.value = 'Enter your Surname';
    } else if (val.trim().isEmpty) {
      surnameError.value = 'Enter your Surname';
    } else if (val.trim().length > 30) {
      surnameError.value = 'Enter valid Surname';
    } else {
      surnameError.value = 'success';
    }
  }

  void validateUserName(String val) {
    username = val;
    update();
    if (val == null) {
      userNameError.value = 'Enter your username';
    } else if (val.trim().isEmpty) {
      userNameError.value = 'Enter your username';
    } else if (val.trim().length > 30) {
      userNameError.value = 'Enter valid username';
    } else {
          userNameError.value = 'success';

    }
  }

  void validateEmail(String val) {
    email = val;
    update();
    if (val == null) {
      emailError.value = 'Enter your email address';
    } else if (val.trim().isEmpty) {
      emailError.value = 'Enter your email address';
    } else if (!val.trim().isEmail) {
      emailError.value = 'Enter valid email address';
    } else {
          emailError.value = 'success';

    }
  }




  void validatePassword(String val) {
    password = val;
    update();
    if (val == null) {
      passwordError.value = 'Enter your password';
    } else if (val.trim().isEmpty) {
      passwordError.value = 'Enter your password';
    } else if (val.trim().length < 6) {
      passwordError.value = 'Your password must have at least 6 letters';
    } else {
      passwordError.value = 'success';
    }
  }

  void validteTandC(bool val) {
    if (val) {
      tcError.value = 'success';
    } else {
      tcError.value = 'review';
    }
  }

//! is loading
  bool isLoadimg = false;
  void isLoading(bool isLoading) {
    isLoadimg = isLoading;
    update();
  }




  insert1(String em){
    var insrt=SignUpData(name: name,surname: surname,username: username,email1: email);
   SignUpStore().setToDatabase1(insrt,em);
   print("values inserted");

  }




  void validateRahul() async{
    var caption = 'Registration failed';

    validateName(name);
    validateSurname(surname);
    validateEmail(email);
    validateUserName(username);
    validatePassword(password);



    List a = email.split("@");
    String b= a[0];



    if (nameError.value != 'success') {
          showMyErrorSnakBar(title: nameError.value, caption: caption);
        } else if (surnameError.value != 'success') {
          showMyErrorSnakBar(title: surnameError.value, caption: caption);
        } else if (emailError.value != 'success') {
          showMyErrorSnakBar(title: emailError.value, caption: caption);
        } else if (userNameError.value != 'success') {
          showMyErrorSnakBar(title: userNameError.value, caption: caption);
        } else if (passwordError.value != 'success') {
          showMyErrorSnakBar(title: passwordError.value, caption: caption);
        }else {
      botLoading();
      isLoading(true);
      try {
        final newUser = await _auth.createUserWithEmailAndPassword(
            email: email, password: password);
        if(newUser!= null)
        {
          insert1(b);

          Get.offAll(() => HomeScreen(),transition: Transition.cupertino,curve:Curves.easeInOutSine, );
          signUpSuccessToast('Registration Successful');
        }
      }
      catch(e){
        BotToast.closeAllLoading();
        signUpFailedToast("Registration Failed");
        print("hi");
        print(e);
      }
    }
  }



  Future<void> signUpFailedToast(String error) {
    botSuccessMsg(sucessMsg: error, isSuccess: false);
    return Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }

  void signUpSuccessToast(String msg) {
    botSuccessMsg(sucessMsg: msg, isSuccess: true);
    Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }





  double scrollOffset = 0;
  double maxScrollPosition = 0;
  double bgImageHeight = 0;
  double scrollOpacityBg = 1.0;
  double scrollOpacityProfile = 1.0;

  @override
  void onInit() {
    super.onInit();
    listenToScroll();
    heightBuilder();
    animatedOpacityBuilder();
  }

  void listenToScroll() {
    scrollController.addListener(() {
      // print(scrollController.offset);
      scrollOffset = scrollController.offset;
      maxScrollPosition = scrollController.position.maxScrollExtent;
      heightBuilder();
      animatedOpacityBuilder();
    });
  }

  void heightBuilder() {
    var height = (Get.height / 4);
    if (scrollOffset >= height) {
      bgImageHeight = 0;
    } else {
      bgImageHeight = height - scrollOffset;
    }
    update();
    // print(bgImageHeight);
  }

  void animatedOpacityBuilder() {
    scrollOpacityBg =
        (1 - (scrollOffset / maxScrollPosition) * 2).clamp(0.0, 1.0);
    scrollOpacityProfile =
        (1 - (scrollOffset / maxScrollPosition) * 20).clamp(0.0, 1.0);
  }

  void resetOnDispose() {
    scrollOffset = 0;
    maxScrollPosition = 0;
    bgImageHeight = 0;
    scrollOpacityBg = 1.0;
    scrollOpacityProfile = 1.0;
    update();
  }






}


