import 'dart:convert';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:sportsgram/firebase/createEventData.dart';

class GetMatchController extends GetxController {

  var matchList = List<Match>().obs;


  @override
  void onInit() {
    getMatchList();
    super.onInit();
  }

  void getMatchList()async {
    if (await MatchStore().database() != null) {

      for (var matches in await MatchStore().getFromDb()) {
        matchList.add(matches);
      }
    }
  }
}

