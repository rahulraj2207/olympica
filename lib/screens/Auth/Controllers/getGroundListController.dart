import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';


class GetListControllerU extends GetxController {
  var groundList = List<Ground>().obs;

  @override
  void onInit() {

    getgroundList();

    super.onInit();
  }


  void getgroundList()async {
    if (await GroundStore().databaseU() != null) {

      for (var grounds in await GroundStore().getFromDbU()) {
        print(grounds);
        groundList.add(grounds);
      }
    }
  }
}


