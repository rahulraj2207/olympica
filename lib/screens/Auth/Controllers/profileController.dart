import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'package:sportsgram/controller/UserDataController.dart';
// import 'package:sportsgram/models/profileDataModel.dart';
// import 'package:sportsgram/screens/Home/controllers/profileDataController.dart';
// import 'package:sportsgram/services/profileApi.dart';

class ProfileController extends GetxController {

  // DateTime selectedDateTime;
  //
  // void changeSelectedDate(DateTime dateTime) {
  //   selectedDateTime = dateTime;
  //   update();
  // }

  final scrollController = ScrollController();
  double scrollOffset = 0;
  double maxScrollPosition = 0;
  double bgImageHeight = 0;
  double scrollOpacityBg = 1.0;
  double scrollOpacityProfile = 1.0;

  @override
  void onInit() {
    super.onInit();
    listenToScroll();
    heightBuilder();
    animatedOpacityBuilder();
  }



  void listenToScroll() {
    scrollController.addListener(() {
      // print(scrollController.offset);
      scrollOffset = scrollController.offset;
      maxScrollPosition = scrollController.position.maxScrollExtent;
      heightBuilder();
      animatedOpacityBuilder();
    });
  }

  void heightBuilder() {
    var height = (Get.height / 4);
    if (scrollOffset >= height) {
      bgImageHeight = 0;
    } else {
      bgImageHeight = height - scrollOffset;
    }
    update();
    // print(bgImageHeight);
  }

  void animatedOpacityBuilder() {
    scrollOpacityBg =
        (1 - (scrollOffset / maxScrollPosition) * 2).clamp(0.0, 1.0);
    scrollOpacityProfile =
        (1 - (scrollOffset / maxScrollPosition) * 20).clamp(0.0, 1.0);
  }

  void resetOnDispose() {
    scrollOffset = 0;
    maxScrollPosition = 0;
    bgImageHeight = 0;
    scrollOpacityBg = 1.0;
    scrollOpacityProfile = 1.0;
    update();
  }
}
