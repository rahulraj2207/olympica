import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
class JoinMatch extends StatefulWidget {

  JoinMatch(
      {
        this.keyy,
       });
  String keyy;





  @override
  _JoinMatchState createState() => _JoinMatchState();
}

class _JoinMatchState extends State<JoinMatch> {

  final TextEditingController name = TextEditingController();
  final TextEditingController teamname = TextEditingController();


  final TextEditingController phone = TextEditingController();


  final _form = GlobalKey<FormState>(); //for storing form state.

  void _saveForm() {
    final isValid = _form.currentState.validate();
    print(isValid);

    if (isValid) {
        MatchStore().updateJoin(widget.keyy);
      signUpSuccessToast1('Successfully Joined ');
      Get.back();
      return;
    }
  }
  
  
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return  Scaffold(
      appBar: WhiteAppBar(
        title: "Join Match",
      ),
      body: Form(
        key: _form, //assigning key to form

        child: ListView(
          children: <Widget>[

            SizedBox(height: 40),

            TextFormField(
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.withOpacity(.3),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                border: InputBorder.none,
                hintText: "Name",
              ),
              validator: (text) {
                if (!text.isNotEmpty) {
                  return "Enter valid name";
                }
                return null;
              },
            ),
            SizedBox(height: 25,),


            TextFormField(
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.withOpacity(.3),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
                border: InputBorder.none,
                hintText: "Team Name",
              ),              validator: (text) {
                if (!text.isNotEmpty) {
                  return "Enter a Team Name";
                }
                return null;
              },
            ),
            SizedBox(height: 25,),


            TextFormField(
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey.withOpacity(.3),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),

                  border: InputBorder.none,
                  hintText: "Phone",
              ),
              validator: (text) {
                print(text.isNumericOnly);
                if (!(text.length > 9) && text.isNotEmpty) {
                  return "Enter a valid mobile number";
                }
                return null;
              },
            ),

            SizedBox(height: 25,),


            Align(
              alignment: Alignment.center,
              child: buildMyButton(
                onpressed:
                    () {

                      _saveForm();


                },
                text: 'Join',
                width:  size.width/1.1,
                height: 60,
              ),
            ),


          ],
        ),
      ),
    );

  }
}





Widget buildTxtHeading1(String text){
  return Align(
      alignment: Alignment.centerLeft,
      child: Text(
        text,
        style:TextStyle(fontSize: 20,fontWeight: FontWeight.w800,color: Colors.black.withOpacity(.4)),
      ));
}




void signUpSuccessToast1(String msg){
  botSuccessMsg(sucessMsg: msg, isSuccess: true);
  Future.delayed(const Duration(seconds: 3))
      .then((value) => BotToast.closeAllLoading());
}


