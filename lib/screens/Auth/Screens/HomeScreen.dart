

//
// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(child: Text('hii22'),);
//   }
// }
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/createEvent1.dart';
import 'package:sportsgram/screens/Auth/Screens/matches.dart';
import 'package:sportsgram/screens/Auth/Screens/searchScreen.dart';
import 'package:sportsgram/screens/Auth/Screens/eventsScreen.dart';
import 'package:sportsgram/screens/Auth/Screens/profileScreen.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/screens/Auth/widgets/bottomNavBar.dart';
import 'package:firebase_auth/firebase_auth.dart';


String userEmail;

class HomeScreen extends StatefulWidget {





  @override
  _HomeScreenState createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {




  final _auth = FirebaseAuth.instance;

  User loggedInUser;

  void getCurrentUser()async{
    try{
      final user = await _auth.currentUser;
      if(user!= null){
        loggedInUser = user;
        String el = loggedInUser.email;
        List uE = el.split("@");
        userEmail= uE[0];
      }
    }catch(e){
      print(e);
    }

  }





  @override
  void initState() {
    getCurrentUser();
    Firebase.initializeApp().whenComplete(() {
      print("completedhome");
      setState(() {});
    });

super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final homePageCtr = Get.put(HomePageController());
    bool isKeyboardOpend() {
      return (MediaQuery.of(context).viewInsets.bottom == 0.0);
    }
    final size = MediaQuery.of(context).size;
    print(MediaQuery.of(context).viewInsets.bottom);
    return SafeArea(
      top: false,
      child: Scaffold(
        body: Stack(
          children: [
            PageView(
              controller: homePageCtr.pageController,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                MatchesPage(),
                SearchScreen(),
                CreateEvent1(),
                ProfileScreen(userEmail: userEmail),

              ],
            ),
            GetBuilder<HomePageController>(
              builder: (controller) => AnimatedPositioned(
                  duration: const Duration(milliseconds: 600),
                  bottom: controller.hideBottomWidgets
                      ? -size.height
                      : (isKeyboardOpend() ? 0 : -size.height),
                  left: 0,
                  right: 0,
                  child: BottomNavBar()),
            ),
          ],
        ),
        // bottomNavigationBar:
      ),
    );
  }
}
