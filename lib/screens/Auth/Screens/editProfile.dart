import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:flutter/cupertino.dart';


class EditProfilePage extends StatefulWidget {
  // Datum data;
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController name = TextEditingController();
  TextEditingController about = TextEditingController();
  TextEditingController email = TextEditingController();
  String propic = 'q';
   List<String> intrests;
  bool spin = false;
  // late File i;
  // Future<String> _pickSaveImage(String imageId) async {
  //   var pick = await ImagePicker.platform
  //       .pickImage(source: ImageSource.gallery, imageQuality: 85);
  //   // ImagePicker.pickImage(source: ImageSource.gallery);
  //
  //   if (pick == null) {
  //     return "null";
  //   } else {
  //     File imageFile = File(pick.path);
  //     setState(() {
  //       i = imageFile;
  //     });
  //     try {
  //       var ref =
  //       FirebaseStorage.instance.ref().child(getUid()).child("image.jpg");
  //       var d = await ref.putFile(imageFile);
  //       String url = await d.ref.getDownloadURL();
  //       return url;
  //     } catch (e) {
  //       print(e);
  //       return "null";
  //     }
  //   }
  // }

  @override
  void initState() {
    super.initState();
    name.text = "";
    about.text = "";
    email.text = "";
    propic="https://121quotes.com/wp-content/uploads/2020/03/cr7-683x1024.jpg";
    intrests = ["football","cricket","chess"];
  }

  GlobalKey<ScaffoldState> key = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: spin,
      child: Scaffold(
        appBar:  WhiteAppBar(
          title: 'Profile settings',
        ),
        key: key,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        width: 180,
                        height: 190,
                        child: Stack(
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  height: 170,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image:
                                          // i !=null
                                          //     ?
                                          NetworkImage(propic)
                                        // : FileImage(i),
                                      ),
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: Colors.green.shade900, width: .8)),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    InkWell(
                                      onTap: () async {
                                        setState(() {
                                          spin = true;
                                        });
                                        String url = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Cristiano_Ronaldo_2018.jpg/220px-Cristiano_Ronaldo_2018.jpg";
                                        setState(() {
                                          spin = false;
                                        });
                                        if (url != null) {
                                          setState(() {
                                            propic = url;
                                          });
                                        }
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(10),
                                        decoration: BoxDecoration(
                                          color: Colors.green.shade900,
                                          borderRadius:
                                          BorderRadius.circular(60),
                                        ),
                                        child: Icon(
                                          FontAwesomeIcons.pencilAlt,
                                          size: 20,
                                          color: Colors.yellow,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: Colors.green.shade900, width: .8)),
                        child: TextFormField(
                          controller: name,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            hintText: 'Name',
                            hintStyle: TextStyle(
                              color: Color(0xffFF9E40),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: Colors.green.shade900, width: .8)),
                        child: TextFormField(
                          controller: about,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            hintText: 'A little about you!',
                            hintStyle: TextStyle(
                              color: Color(0xffFF9E40),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: Colors.green.shade900, width: .8)),
                        child: TextFormField(
                          controller: email,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            hintText: 'email address',
                            hintStyle: TextStyle(
                              color: Color(0xffFF9E40),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 40),
                        child: Row(
                          children: [
                            Icon(
                              Icons.star,
                              size: 22,
                              color: Color(0xffFF9E40),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              'INTERESTS',
                              style: TextStyle(
                                  color: Color(0xff41287B),
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 60),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (intrests.contains('Football')) {
                                intrests.remove('Football');
                              } else {
                                intrests.add('Football');
                              }
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 110,
                            decoration: BoxDecoration(
                              color: intrests.contains('Football')
                                  ? Color(0xffFF9E40)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Color(0xff41287B), width: .8),
                            ),
                            child: Center(
                              child: Text(
                                'FOOTBALL',
                                style: TextStyle(
                                  color: Color(
                                    0xff41287B,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (intrests.contains('Cricket')) {
                                intrests.remove('Cricket');
                              } else {
                                intrests.add('Cricket');
                              }
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 110,
                            decoration: BoxDecoration(
                              color: intrests.contains('Cricket')
                                  ? Color(0xffFF9E40)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: Color(0xff41287B), width: .8),
                            ),
                            child: Center(
                              child: Text(
                                'CRICKET',
                                style: TextStyle(
                                  color: Color(
                                    0xff41287B,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 100,
                        height: 35,
                        decoration: BoxDecoration(
                          color: Color(0xffFF9E40),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Color(0xffFF9E40),
                          ),
                        ),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            'CANCEL',
                            style: TextStyle(color: Color(0xff41287B)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        // margin: EdgeInsets.only(
                        //   right: 40,
                        // ),
                        width: 140,
                        height: 35,
                        decoration: BoxDecoration(
                          color: Color(0xffFF9E40),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: MaterialButton(
                          onPressed: () async {
                            setState(() {
                              spin = true;
                            });
                            // String msg = await updateProfileApi(
                            //     name.text,
                            //     about.text,
                            //     email.text,
                            //     propic,
                            //     widget.data.location,
                            //     intrests);

                            setState(() {
                              spin = false;
                            });
                            // key.currentState
                            //     .showSnackBar(SnackBar(content: Text(msg)));
                            // await Future.delayed(Duration(seconds: 2));
                            // Navigator.pushAndRemoveUntil(
                            //     context,
                            //     MaterialPageRoute(
                            //       builder: (context) => NavigationBar(
                            //         count: 1,
                            //       ),
                            //     ),
                            //         (route) => false);
                          },
                          child: Text(
                            'SAVE CHANGES',
                            style: TextStyle(color: Color(0xff41287B)),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
class WhiteAppBar extends StatelessWidget implements PreferredSizeWidget {
  const WhiteAppBar({
    Key key,
    @required this.title,
  }) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: TextStyle(color: Colors.black),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(
          CupertinoIcons.back,
          color: Colors.grey,
        ),
        onPressed: () {
          print('hi');
          Get.back();
        },
      ),
      elevation: 0,
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}


