import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/utils/utils//theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_rounded),
          onPressed: () => Get.back(),
        ),
      ),
      body: Column(
        children: [
          const Expanded(
            child: Align(
              alignment: Alignment.center,
              child: SingleChildScrollView(
                child: ForgotPasswordBody(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: buildMyButton(
              onpressed: () {
                Get.focusScope.unfocus();
                // final forgotPassCtr = Get.find<ForgotPasswordController>();
                // forgotPassCtr.resetPassword();
              },
              text: 'Reset Password',
              width: Get.width > 600 ? 550 : Get.width,
              height: 60,
            ),
          )
        ],
      ),
    );
  }
}

class ForgotPasswordBody extends StatelessWidget {
  const ForgotPasswordBody({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: Get.height * 0.03,
        ),
        buildheadings(
          style: TextStyle(fontSize: 20),
            text: 'Enter your registered ID',
            height: 50,
            alignment: const Alignment(0, 0)),

        SizedBox(
          height: Get.height * 0.04,
        ),
        const ForgotTxtField(),
        // GetX<ForgotPasswordController>(
        //     builder: (controller) => buildTextFildErrorBox(
        //         errorMsg: controller.forgetEmailError.value)),
      ],
    );
  }
}

class ForgotTxtField extends StatelessWidget {
  const ForgotTxtField({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TextField(
        // controller: forgetPassCtr.forgetTextCtr,
        autofocus: true,
        keyboardType: TextInputType.emailAddress,
        // onChanged: forgetPassCtr.validateEmail,
        decoration: InputDecoration(
          fillColor: Colors.white,
          hintText: 'email',
          prefixIcon: const Icon(Icons.mail),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.green,
              width: .3,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2),
            borderRadius: BorderRadius.circular(5),
          ),
        ),
      ),
    );
  }
}
