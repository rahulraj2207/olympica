import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/UserDataController.dart';
import 'package:sportsgram/screens/Auth/Controllers/loginController.dart';
import 'package:sportsgram/owner/ownerLogin.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/screens/Auth/widgets/myTextFiled.dart';
import 'package:bot_toast/bot_toast.dart';
import 'forgotPasswordScreen.dart';
import 'package:sportsgram/screens/Auth/widgets/signUpNoteBody.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}


const colorizeColors = [
  Colors.purple,
  Colors.blue,
  Colors.yellow,
  Colors.red,
];

const colorizeTextStyle = TextStyle(
    fontSize: 40.0,fontWeight: FontWeight.bold
);

class _LoginScreenState extends State<LoginScreen> {

@override
  void initState() {
  Firebase.initializeApp().whenComplete(() {
    print("completed");
    setState(() {});
  });
  super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Get.put(UserDataController());

    return GestureDetector(
      onTap: () {
        if (Get.focusScope.hasFocus) {
          Get.focusScope.unfocus();
        }
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            width: Get.width,
            height: Get.height,
            decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg3.jpeg"),
              fit: BoxFit.cover,
            ),
          ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [


                    Align(alignment: Alignment.topRight,child: Text("Owner",style: TextStyle(
                        color: Colors.white,fontSize: 15,fontWeight: FontWeight.w600
                    ),)),

                    SizedBox(height: Get.height * 0.01),

                    Align(  alignment: Alignment.topRight,
                      child: GestureDetector(
                          onTap: (){
                            Get.to(OwnerLogin(),
                              duration: const Duration(milliseconds: 600),
                              curve: Curves.easeInOutSine,
                              transition: Transition.upToDown,);

                          },

                          child: Icon(Icons.switch_account_sharp,color: Colors.white,)),
                    ),

                    SizedBox(height: Get.height * 0.10),

                  // Center(
                  //   child: SizedBox(
                  //     width: 300.0,
                  //     child: DefaultTextStyle(
                  //       style: const TextStyle(
                  //         fontSize: 35.0,
                  //       ),
                  //       child: AnimatedTextKit(
                  //       animatedTexts: [
                  //           TypewriterAnimatedText('O L Y M P I C A',speed: Duration(),textStyle: TextStyle(fontWeight: FontWeight.bold)),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),


                    Center(
                      child: SizedBox(
                        width: 300.0,
                        child: AnimatedTextKit(
                          animatedTexts: [
                            ColorizeAnimatedText(
                              'O L Y M P I C A',
                              textStyle: colorizeTextStyle,
                              colors: colorizeColors,
                            ),
                          ],
                          isRepeatingAnimation: true,

                        ),
                      ),
                    ),


                    // buildheadings(text: 'O L Y M P I C A', height: 60,style: TextStyle(
                    //   fontSize: 50, color: Colors.white.withOpacity(.9),fontWeight: FontWeight.bold,
                    // )),

                    SizedBox(height: Get.height * 0.08),

                    // Center(
                    //   child:
                    //   // Image.asset(
                    //   //   'assets/images/sportslogo.png',
                    //   //   width: 200,
                    //   // ),
                    // ),
                    // SizedBox(height: Get.height * 0.15),
                    Row(
                      children: [
                        Expanded(
                          child: MyTextFiled(
                            enums: TextFieldSwitchers.lEmail,
                            hintText: 'Email',
                          ),
                        ),
                        GetX<LoginController>(
                          builder: (controller) => buildErrorIcon(
                              onPressed: () {
                                BotToast.showText(
                                  text: controller.emailUserMobError.value,
                                    align: Alignment.center
                                );
                              },
                              errorMsg: controller.emailUserMobError.value
                            ),
                        )
                      ],
                    ),
                    SizedBox(height: Get.height * 0.03),
                    Row(
                      children: [
                        Expanded(
                          child: MyTextFiled(
                            enums: TextFieldSwitchers.lPassword,
                            hintText: 'Password',
                          ),
                        ),
                        GetX<LoginController>(
                          builder: (controller) => buildErrorIcon(
                              onPressed: () {
                                BotToast.showText(
                                  text: controller.passwordError.value,
                                  align: Alignment.center
                                );
                              },
                              errorMsg: controller.passwordError.value
                            ),
                        ),
                      ],
                    ),
                    // SizedBox(height: Get.height * 0.03),

                    SizedBox(height: Get.height * 0.03),
                    Align(
                      alignment: Alignment.center,
                      child: buildMyButton(
                        onpressed: () {

                          Get.focusScope.unfocus();
                          Get.find<LoginController>().validateRahul();

                        },
                        text: 'Sign in',
                        width: Get.width > 600 ? 550 : Get.width,
                        height: 60,
                      ),
                    ),
                    SizedBox(height: Get.height * 0.03),
                    Align(  alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () {
                          Get.focusScope.unfocus();
                          Get.to(
                            ForgotPasswordScreen(),
                            transition: Transition.fadeIn,
                          );
                        },
                        child: Text(
                          'Forgot Password?',
                          style: TextStyle(fontSize: 17,fontWeight: FontWeight.bold,color: Colors.white),
                        ),
                      ),
                    ),
                    SizedBox(height: Get.height * 0.09),

                     Align(
                      alignment: Alignment.bottomCenter,
                      child: SignUpNoteBody(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


