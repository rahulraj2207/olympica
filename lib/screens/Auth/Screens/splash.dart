import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:get/get.dart';

import 'package:sportsgram/screens/Auth/Screens/loginScreen.dart';
import 'package:sportsgram/screens/Auth/Screens/HomeScreen.dart';


 bool islogged = false;
class splash extends StatefulWidget {
  @override
  _splashState createState() => _splashState();
}




class _splashState extends State<splash> {





  setShare() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getBool('islogged') != null){

       islogged = sharedPreferences.getBool('islogged');
    }
  }


  @override
  void initState() {

  setShare();
  Future.delayed(Duration(milliseconds: 100)).then((value) async{
    if(islogged){
      Get.to(HomeScreen());

    }else{
      Get.to(LoginScreen());
    }

  }
  );
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
