import 'package:flutter/material.dart';
import 'package:sportsgram/screens/Auth/Screens/bookpage3.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';

class BookPage2 extends StatefulWidget {
  BookPage2(
      {
        this.venue,
        this.date,
        this.selectedslots,
        this.players,
        this.category,
        this.contact,
        this.totalAmount
      });
  String venue;
  String date;
  String selectedslots;
  int players;
  String totalAmount;
  String category;
  String contact;


  @override
  _BookPage2State createState() => _BookPage2State();
}

class _BookPage2State extends State<BookPage2> {

  int total;

  GlobalKey<ScaffoldState> key = GlobalKey();
  @override
  void initState() {

    total = (int.parse(widget.totalAmount)) * widget.players;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const WhiteAppBar(
      title: 'Booking Details',
    ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(right: 20, left: 20, top: 20),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                decoration: BoxDecoration(
                  color: Colors.green.withOpacity(.7),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Venue',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 55,
                        ),
                        Text(widget.venue,
                          // widget.venue ?? "",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.white,
                      thickness: .8,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Category',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 38,
                        ),
                        Text(
                          widget.category ?? '',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),

                      ],
                    ),
                    Divider(
                      color: Colors.white,
                      thickness: .8,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Date',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 67,
                        ),
                        Text(
                          // "date",
                          widget.date.split(' ')[0] ?? '',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.white,
                      // height: 3,
                      thickness: 1,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Time',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 63,
                        ),
                        Text(
                          widget.selectedslots ?? '',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),

                      ],
                    ),
                    Divider(
                      color: Colors.white,
                      // height: 3,
                      thickness: 1,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Players',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          // '8',
                          widget.players.toString() ?? "",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Divider(
                      color: Colors.white, thickness: 1,
                      // height: 3,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          'Total\nAmount',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Text(
                          total.toString() ,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xff41287B),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                'YOU PAY THE ADVANCE TO BOOK YOUR SLOT, \nREMAINING AMOUNT TO BE PAID AT THE VENUE',
                style: TextStyle(
                  color: Color(0xff41287B),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              // Text(
              //   'PLEASE READ OUR',
              //   style: TextStyle(
              //     color: Color(0xff41287B),
              //     fontWeight: FontWeight.bold,
              //   ),
              // ),
              // Text(
              //   'CANCELLATION/CHANGE POLICY',
              //   style: TextStyle(
              //     color:Colors.green,
              //     fontWeight: FontWeight.bold,
              //   ),
              // ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 25,
                ),
                decoration: BoxDecoration(
                  color:Colors.green,
                  borderRadius: BorderRadius.circular(15),
                ),
                height: 80,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '₹' ,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      total.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                    Container(
                      width: 125,
                      height: 35,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: MaterialButton(
                        onPressed: ()  {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BookPage3(
                                selectedslots: widget.selectedslots,
                                date: widget.date,
                                players: widget.players,
                                venue: widget.venue,
                                category: widget.category,
                                contact: widget.contact,
                                total: total,
                              ),
                            ),
                          );
                        },
                        child: Text(
                          'PAY NOW',
                          style: TextStyle(
                            color: Color(
                              0xff41287B,
                            ),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}