
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/HomeScreen.dart';


class HostingConfirm extends StatefulWidget {

  HostingConfirm(
      {
        this.image,
        this.venue,
        this.date,
        this.time,
        this.players,
        this.cate,
        this.teamN,
        this.host,
        this.phone,

      });
  File image;
  String phone;
  String host;
  String teamN;
  String venue;
  String date;
  String time;
  int players;
  String cate;


  @override
  _HostingConfirmState createState() => _HostingConfirmState();
}

class _HostingConfirmState extends State<HostingConfirm> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            CupertinoIcons.back,
            color: Colors.black,
          ),
          onPressed: (){
            Get.to(HomeScreen());
          },
        ),
      ),
      backgroundColor: Colors.grey,
      body: SingleChildScrollView(
        child: Container(
          color:Colors.grey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 20,
              ),


              SizedBox(
                height: 10,
              ),
              Stack(
                children: [
                  Container(
                    height: 400,
                    width: double.infinity,
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    padding: EdgeInsets.symmetric(
                        horizontal: 25, vertical: 25),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [




                        Text(widget.cate??"",
                          // widget.b_id ?? '',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                        SizedBox(height: 15,),
                        Text("at",style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(height: 8,),

                        Text(widget.venue??"",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                        SizedBox(height: 15,),

                        Text("on",style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(height: 8,),

                        Text(widget.date??"",
                          // snapshot.data!.turfDetails.turfName ?? '',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                        SizedBox(height: 8,),

                        Text(widget.time??"",
                          // snapshot.data!.turfDetails.turfName ?? '',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                        SizedBox(height: 10,),

                        Text("Hosted by",style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),

                        SizedBox(height: 8,),



                        Text(widget.host??"",
                          // snapshot.data!.turfDetails.turfName ?? '',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                          ),
                        ),
                        SizedBox(height: 10,),






                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 190,
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.grey,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: 190,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            CircleAvatar(
                              backgroundColor: Colors.grey,
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 25),
                padding:
                EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.host??"",
                      // snapshot.data!.turfDetails.turfName ?? '',
                      style: TextStyle(
                          color: Color(0xff41287B),
                          fontSize: 15,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.location_on,
                          size: 23,
                          color: Color(0xff41287B),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          widget.venue??"",
                          // snapshot.data!.turfDetails.location ?? '',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.phone,
                          size: 23,
                          color: Color(0xff41287B),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          widget.phone??"",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );

  }
}
