import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/firebase/bookingTime.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/firebase/signUpData.dart';
import 'package:sportsgram/screens/Auth/Controllers/getGroundListController.dart';
import 'package:sportsgram/screens/Auth/Screens/hostingConfirm.dart';
import 'package:sportsgram/screens/Auth/widgets/ratingStar.dart';
import 'package:sportsgram/utils/utils/assets/assetsUtils.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/owner/ownerPage.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;

File _image;
PickedFile _image1;
String _uploadedFileURL;
var _isExpanded = false;

class CreateEvent1 extends StatefulWidget {

  @override
  _CreateEvent1State createState() => _CreateEvent1State();
}

class _CreateEvent1State extends State<CreateEvent1> {

  int _expandedIndex;
  String userEmail;
  String userNa;

  final _auth = FirebaseAuth.instance;

  User loggedInUser;


  void getCurrentUser()async{
    try{
      final user = await _auth.currentUser;
      if(user!= null){
        loggedInUser = user;
        List uE = loggedInUser.email.split("@");
        userEmail= uE[0];
      }
    }catch(e){
      print(e);
    }
  }
  var userList = List<SignUpData>().obs;
  void getUSER1(String emm)async {
    if (await SignUpStore().database1(emm) != null) {

      for (var user in await SignUpStore().getFromDb1(emm)) {
        userList.add(user);
      }


    }
  }



  Future uploadFile() async {
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child('_uploadedFileURL/${Path.basename(_image.path)}}');
    UploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
        print("hihi");
        print(_uploadedFileURL);
      });
    });
  }


  final GetListControllerU getListController = Get.find();

  final TextEditingController matchName = TextEditingController();
  final TextEditingController grndctr = TextEditingController();
  final TextEditingController hostName = TextEditingController();
    final TextEditingController time = TextEditingController();
     final TextEditingController phone = TextEditingController();

  final  homepagectr = Get.put(HomePageController());


  List<Match>matchList=[];


  List<int> players = [1,2,3,4,5,6,7,8,9,10,11];



  List<String> sport = ['Cricket',
    'Football',
    'Basketball',
    'Volleyball',
    'Table tennis',
    'Tennis',
    'Badminton',
    'Chess',
    'Dodgeball',
    'Hockey']; // Option 2
  String selectedSport; // Option 2

  String selectedGround;
  String category;

 int selectedPlayers=1;

  final ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    Firebase.initializeApp();
    getCurrentUser();
    getUSER1(userEmail);
    setState(() {

    });
    print("cretevnt");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    var sizedBox002 = SizedBox(height: size.height * 0.02);


    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  buildAppBar(
        title: Text('Host Match',style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w500,
        ),),
      ),
      body: SingleChildScrollView(
          // physics: ScrollPhysics(),

          padding:  EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            children: [
              sizedBox002,

              // Picker(),
              buildTxtHeading('Pick a play space'),


              sizedBox002,

              Container(
                height: 200,
                width: double.infinity,
                child: Obx(
                      () {
                    print("insideobx");
                    if (getListController.groundList.isEmpty)
                      return Center(child: CircularProgressIndicator());
                    else
                      return ListView.builder(
                        controller: _scrollController,
                        padding: const EdgeInsets.only(bottom: 25, top: 15),
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: getListController.groundList.length,
                        itemBuilder: (context, i)
                        {


                          print("insidelisvw");
                          return Padding(
                            padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15),

                            child: GestureDetector(
                              onTap: (){

                                BotToast.showText(
                                    text: getListController.groundList[i].gname??"",
                                    align: Alignment.center
                                );

                                setState(() {
                                  grndctr.text= getListController.groundList[i].gname;
                                  selectedGround= getListController.groundList[i].gname;
                                  selectedSport=getListController.groundList[i].gabout;
                                  hostName.text = userList[0].name;
                                });


                                // Get.to(BookPage1(loc: getListController.groundList[i].gname,));
                              },
                              child: EventsCard2(
                                opens: getListController.groundList[i].open,
                                about: getListController.groundList[i].gabout,
                                groundName: getListController.groundList[i].gname,
                                eventRating: 2,
                              ),
                            ),
                          );


                        },
                      );
                  },
                ),
              ),

              sizedBox002,


              TextFieldProfileEdit(
                readonly: true,
                controller: grndctr,
              ),

              sizedBox002,
              buildTxtHeading('Host Name'),

              TextFieldProfileEdit(
                controller: hostName,
              ),


              sizedBox002,
              buildTxtHeading('Select Date'),
              GetBuilder<HomePageController>(
                builder: (ctr) => _listTileDateTime(
                  context: context,
                  iconAsset: AssetsUtils.calendar,
                  title: 'Select Date',
                  traling: ctr.selectedDateTime == null
                      ? ''
                      : DateFormat()
                      .add_MMMEd()
                      .format(ctr.selectedDateTime),
                  onPressed: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2020),
                      lastDate: DateTime(2022),
                    ).then((dateTime) {
                      if (dateTime != null) {
                        homepagectr
                            .changeSelectedDate(dateTime);
                      }
                    });
                  },
                ),
              ),
              sizedBox002,

              sizedBox002,
              buildTxtHeading('Time Slots'),

              TextFieldProfileEdit(
                readonly: true,
                controller: time,
              ),
              sizedBox002,
              sizedBox002,


              Container(
                height: 80,
                width: size.width,
                child: ListView.builder(
                    itemCount: slots.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (ctx, index) {
                      var slot = slots[index];
                      return GestureDetector(
                          onTap: () {

                            getCurrentUser();
                            print(userEmail);


                            String tim = homepagectr.selectedDateTime.toString()+time.text;

                              setState(() {
                                TimeStore().getTime(tim);
                                time.text=slot.time;
                                booked=time.text;
                                print(booked);
                                _expandedIndex = index;
                              });


                          },
                          child: Container(
                            child: Center(child: Text(slot.time)),
                            width: 80,
                              height: 25,
                              padding: EdgeInsets.all(10),
                              margin: EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                  color: _expandedIndex == index
                                      ? Colors.red
                                      : Colors.green,

                                  borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                              // child: SlotsWidget()
                          )
                      );
                    }
                ),
              ),


              sizedBox002,
              sizedBox002,


              sizedBox002,


              GestureDetector(
                  onTap: (){
                    Get.bottomSheet(
                      ClipRRect(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        child: ColoredBox(
                          color: Colors.white,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                ClipRRect(
                                  borderRadius:
                                  BorderRadius.circular(10),
                                  child: ColoredBox(
                                    color: Colors.grey
                                        .withOpacity(0.3),
                                    child: const SizedBox(
                                      height: 5,
                                      width: 40,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets
                                      .symmetric(
                                      vertical: 12.0,
                                      horizontal: 4),
                                  child: Text("Maximum Players",style: TextStyle(
                                      color: Colors.black.withOpacity(.9),fontSize: 20,fontWeight: FontWeight.w500
                                  ),),
                                ),
                                SizedBox(
                                    height: size.height/2.5,
                                    child: ListView.builder(
                                      itemCount: players.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return
                                          GestureDetector(onTap: (){

                                            BotToast.showText(
                                                text: players[index].toString()??"",
                                                align: Alignment.center
                                            );
                                            setState(() {
                                              print(players[index]);
                                              selectedPlayers= players[index];
                                            });

                                          },
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Container(
                                                height: 50,
                                                width: 50,
                                                color:  Colors.green.withOpacity(.2),

                                                child: Center(
                                                  child: Text(
                                                    players[index].toString(),style: TextStyle(fontSize: 20),

                                                  ),),),
                                            ),
                                          );
                                      },
                                    )
                                )],
                            ),
                          ),
                        ),
                      ),
                      isScrollControlled: true,
                    );

                  },
                  child :Column(
                    children: [
                      SizedBox(
                        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            buildTxtHeading("Select Maximum Number of Players"),
                            Icon(CupertinoIcons.arrow_right,size: 30,color: Colors.green,),
                          ],
                        ),
                      ),
                      Card(
                        color: cAnccent,
                        child: ListTile(
                          contentPadding: EdgeInsets.only(left: 5,right: 5),
                          leading: Text(
                            selectedPlayers.toString()??"",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 19),
                          ),
                        ),
                      )

                    ],
                  )
              ),
              buildTxtHeading('Team name'),
              sizedBox002,

              TextFieldProfileEdit(
                controller: matchName,

              ),





                sizedBox002,
                                buildTxtHeading('Phone'),

                  TextFieldProfileEdit(
                    controller: phone,
                  ),
              SizedBox(height: size.height * 0.03),

              (
                  selectedGround!=null&&
                      homepagectr.selectedDateTime.toString()!=null&&
                      selectedPlayers!=null&&
                      matchName.text!=null&&
                      hostName.text!=null&&
                      phone.text!=null&&
                      time.text!=null ) ?

              buildMyButton(
                  text: 'Create Match',
                  isScaledown: true,

                  onpressed:  (
                      validateTextField(matchName.text)&&
                      validateTextField(hostName.text)&&
                  validateTextField(phone.text)&&
              selectedGround!=null&&
                      homepagectr.selectedDateTime.toString()!=null&&
                      selectedPlayers!=null&&
                      time.text!=null )? (){


                    List aa= homepagectr.selectedDateTime.toString().split(" ");
                    String bb = aa[0];

                    // uploadFile();
                     insert();
                    insertusermatch();
                     insertTime();
                    Get.to(HostingConfirm(
                        image: _image,
                        cate: selectedSport,
                        venue: selectedGround,
                        date: bb,
                        players: selectedPlayers,
                        teamN: matchName.text,
                        host: hostName.text,
                        phone: phone.text,
                        time: time.text,
                      ));


                    signUpSuccessToast('Match Created');
                    grndctr.clear();
                    matchName.clear();
                    hostName.clear();
                    time.clear();
                    phone.clear();
                    print("haha");


                    // print(_uploadedFileURL);

                    }:null ,
                  borderRadius: BorderRadius.circular(10),
                  height: 60,
                  width: size.width):

              buildMyButton(
              borderRadius: BorderRadius.circular(10),
          height: 60,
          width: size.width,
          text: 'Create Match',
          color: Colors.grey,
          // child: CircularProgressIndicator(),
          isScaledown: true),




    SizedBox(height: 100,)            ],
          )),
    );
  }

  Widget buildTxtHeading(String text) {
    return Align(
        alignment: Alignment.centerLeft,
        child: Text(
          text,
          style:  TextStyle(fontSize: 20,fontWeight: FontWeight.w800,color: Colors.black.withOpacity(.4)),
        ));
  }
  bool b = false;//for join is true or false

  insert(){
    var insrt=Match(image: _uploadedFileURL,mname: matchName.text,hname: hostName.text,mnum: selectedPlayers,mtime: time.text,mdate: homepagectr.selectedDateTime.toString(),mphone: phone.text,mcategory: selectedSport,mground: selectedGround,join: b);
    MatchStore().setToDatabase(insrt);
  }
  insertusermatch(){
    var insrt1=Match(image: _uploadedFileURL,mname: matchName.text,hname: hostName.text,mnum: selectedPlayers,mtime: time.text,mdate: homepagectr.selectedDateTime.toString(),mphone: phone.text,mcategory: selectedSport,mground: selectedGround,join: b);
    MatchStore().setToDatabaseInuser(insrt1,userEmail);
  }

  insertTime(){
    String t = homepagectr.selectedDateTime.toString()+time.text;
   print(t);
    var insrt=Time(time: t);
    TimeStore().setToDatabaseT(insrt);
  }


}







class TextFieldProfileEdit extends StatefulWidget {
  const TextFieldProfileEdit({
    Key key,
    this.maxLines,
    this.minLines,
    this.prefixText,
    this.readonly,
    this.controller,
    this.text,
    this.maxLength,
    this.obsecureText,
    this.contentPading,
    this.newTextField = false,
    this.height,
    this.hintText,
    this.titleStyle,
  }) : super(key: key);

  final int minLines;
  final int maxLines;
  final int maxLength;
  final String prefixText;
  final TextEditingController controller;
  final String text;
  final String hintText;
  final bool obsecureText;
  final EdgeInsetsGeometry contentPading;
  final bool newTextField;
  final bool readonly;

  final double height;
  final TextStyle titleStyle;
  @override
  _TextFieldProfileEditState createState() => _TextFieldProfileEditState();
}

class _TextFieldProfileEditState extends State<TextFieldProfileEdit> {
  @override
  void initState() {
    super.initState();
    widget.controller.text = widget.text;
  }

  @override
  void didUpdateWidget(TextFieldProfileEdit oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.text != widget.text) {
      widget.controller.text = widget.text;
    }
  }
  // @override
  // void didChangeDependencies() {
  //   super.didChangeDependencies();
  //     widget.controller.text = widget.text;

  // }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: widget.newTextField
          ? TextField(
        controller: widget.controller,
        textAlign: TextAlign.center,
        style: widget.titleStyle ??
            Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 30),
        decoration: InputDecoration(
            border: InputBorder.none, hintText: widget.hintText ?? ''),
      )
          : SizedBox(
        height: widget.height,
        child: ColoredBox(
            color: cAnccent,
            child: TextField(
              readOnly: widget.readonly??false,
              autofocus: false,
              decoration: InputDecoration(
                errorText: isUserNameValidate ? "" : null,
                  border: InputBorder.none,
                  contentPadding: widget.contentPading ??
                      const EdgeInsets.symmetric(horizontal: 10),
                  prefixText: widget.prefixText),
              minLines: widget.minLines ?? 1,
              maxLines: widget.maxLines ?? 1,
              controller: widget.controller,
              maxLength: widget.maxLength,
              obscureText: widget.obsecureText ?? false,
            )),
      ),
    );
  }
}



            void signUpSuccessToast(String msg) {
  botSuccessMsg(sucessMsg: msg, isSuccess: true);
  Future.delayed(const Duration(seconds: 3))
      .then((value) => BotToast.closeAllLoading());
}


Card _listTileDateTime(
    {BuildContext context,
      String iconAsset,
      String title,
      String traling,
      Function onPressed}) {
  final size = MediaQuery.of(context).size;
  return Card(
   color: cAnccent,
    child: ListTile(
      contentPadding: EdgeInsets.only(left: 5,right: 5),
      trailing: roundedIconBox(
          icon: AssetsUtils.calendar,
          bgColor: Colors.green.withOpacity(0.1),
          iconColor: Colors.green.shade900,
          width: 40,
          height: 40),
      leading: Text(
        traling,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 19),
      ),
      onTap: onPressed,
    ),
  );
}





class EventsCard2 extends StatelessWidget {
  final String about;
  final String groundName;
  final int eventRating;
  final double distance;
  final String opens;

  const EventsCard2(
      {Key key,
        this.opens,
        this.distance,
        this.about,
        this.eventRating,
        this.groundName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 230,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: Colors.green.withOpacity(0.1),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [

              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        Text(
                          groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        Text(
                          about??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,color: Colors.black.withOpacity(.7)
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),

                        Row(mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "₹",
                              textAlign: TextAlign.left,
                              style: Get.textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                            Text(
                              opens??"",
                              textAlign: TextAlign.left,
                              style: Get.textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ), Text(
                              " /hr",
                              textAlign: TextAlign.left,
                              style: Get.textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                            SizedBox(width: 5,),


                          ],
                        ),





                        Row(mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Rating",style: TextStyle(
                                fontSize: 15
                            ),
                            ),

                            Expanded(
                              child: RatingWithStar(
                                rating: eventRating ?? 0,
                              ),
                            ),

                          ],
                        ),

                        // )
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}



class Slot {
  String time;
  bool isSelected;

  Slot(this.time, this.isSelected);
}

var slots = [
  Slot('06:00PM - 07:00PM', false),
  Slot('07:00PM - 08:00PM', false),
  Slot('08:00PM - 09:00PM', false),
  Slot('09:00PM - 10:00PM', false),
  Slot('10:00PM - 11:00PM', false),
  Slot('11:00PM - 12:00PM', false),

  //... Rest of the items
];
var bks = [];
var booked;

bool isUserNameValidate = false;

bool validateTextField(String userInput) {
  if (userInput.isEmpty) {
      isUserNameValidate = true;
    return false;
  }
    isUserNameValidate = false;

  return true;
}