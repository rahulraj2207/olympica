import 'package:flutter/material.dart';
import 'package:sportsgram/screens/Auth/Screens/bookpage4.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:get/get.dart';
class BookPage3 extends StatefulWidget {

  BookPage3(
      {
        this.venue,
        this.date,
        this.selectedslots,
        this.players,
        this.category,
        this.contact,
        this.total
      });
  String venue;
  String date;
  String selectedslots;
  int players;
  int total;
  String category;
  String contact;

  @override
  _BookPage3State createState() => _BookPage3State();
}

class _BookPage3State extends State<BookPage3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const WhiteAppBar(
        title: 'Your Booking',
      ),
      body: Container(
        margin: EdgeInsets.only(
          top: 30,
          right: 20,
          left: 20,
        ),
        child: InkWell(
          onTap: () {



           Get.to( BookPage4(selectedslots: widget.selectedslots,
             date: widget.date,
             players: widget.players,
             venue: widget.venue,
             category: widget.category,
             contact: widget.contact,
             total: widget.total,));
          },
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: 25,
            ),
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(15),
            ),
            height: 100,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '₹',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  widget.total.toString(),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  'PAYMENT',
                  style: TextStyle(
                      color: Color(0xff41287B),
                      fontSize: 15,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
