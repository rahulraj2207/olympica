import 'dart:async';
import 'dart:ui';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/firebase/bookingTime.dart';
import 'package:sportsgram/screens/Auth/Screens/createEvent1.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/utils/utils/assets/assetsUtils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sportsgram/screens/Auth/Screens/bookpage2.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';


bool col = false;
int _expandedIndex1;
class BookPage1 extends StatefulWidget {

  BookPage1(
      {
        this.name,
        this.rate,
        this.category,
        this.contact,

      });
  String name;
  String rate;
  String category;
  String contact;


  @override
  _BookPage1State createState() => _BookPage1State();
}

class _BookPage1State extends State<BookPage1> {

  final  homepagectr = Get.put(HomePageController());


  final TextEditingController time1 = TextEditingController();

  int no_of_players=0;


  insertTime(){
    String t = homepagectr.selectedDateTime.toString()+time1.text;
    print(t);
    var insrt=Time(time: t);
    TimeStore().setToDatabaseT(insrt);
  }


  @override
  void initState() {

 super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
            appBar:   const WhiteAppBar(
            title: 'Booking Page',
          ),
            body: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [

                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: 20,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [


                            Container(height: size.height/4,
                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                              decoration: BoxDecoration(

                                color: Colors.green.withOpacity(.7),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'Ground name:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 55,
                                      ),
                                      Text(widget.name,
                                        // widget.venue ?? "",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.white,
                                    thickness: .8,
                                  ),

                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'Category:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 70,
                                      ),
                                      Text(
                                       widget.category??"" ,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.white, thickness: 1,
                                    // height: 3,
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'Rate per hr:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 65,
                                      ),
                                      Text(
                                        '₹ ' ,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Text(
                                        widget.rate??"",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    color: Colors.white, thickness: 1,
                                    // height: 3,
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Text(
                                        'Contact ground:',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 50,
                                      ),
                                      Text(
                                        widget.contact ,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: Color(0xff41287B),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),










                            SizedBox(height: 20,),



                            Text('Select Date',style: TextStyle(
                              fontSize: 20
                            ),),
                            GetBuilder<HomePageController>(
                              builder: (ctr) => _listTileDateTime(
                                context: context,
                                iconAsset: AssetsUtils.calendar,
                                title: 'Select Date',
                                traling: ctr.selectedDateTime == null
                                    ? ''
                                    : DateFormat()
                                    .add_MMMEd()
                                    .format(ctr.selectedDateTime),
                                onPressed: () {
                                  showDatePicker(
                                    context: context,
                                    initialDate: DateTime.now(),
                                    firstDate: DateTime(2020),
                                    lastDate: DateTime(2022),
                                  ).then((dateTime) {
                                    if (dateTime != null) {
                                      homepagectr
                                          .changeSelectedDate(dateTime);
                                    }
                                  });
                                },
                              ),
                            ),
                            SizedBox(height: 20,),


                            Text('Time slots',style: TextStyle(
                                fontSize: 20
                            ),),
                            SizedBox(height: 20,),

                            TextFieldProfileEdit(
                              readonly: true,
                              controller: time1,
                            ),

                            SizedBox(height: 20,),

                            Container(
                              height: 80,
                              width: size.width,
                              child: ListView.builder(
                                  itemCount: slots.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder: (ctx, index) {
                                    var slot = slots[index];
                                    return GestureDetector(
                                        onTap: () {

                                          String tim = homepagectr.selectedDateTime.toString()+time1.text;

                                          setState(() {
                                            TimeStore().getTime(tim);
                                            time1.text=slot.time;
                                            booked=time1.text;
                                            print(booked);
                                            _expandedIndex1 = index;
                                          });


                                        },
                                        child: Container(
                                          child: Center(child: Text(slot.time)),
                                          width: 80,
                                          height: 25,
                                          padding: EdgeInsets.all(10),
                                          margin: EdgeInsets.all(4),
                                          decoration: BoxDecoration(
                                              color: _expandedIndex1 == index
                                                  ? Colors.red
                                                  : Colors.green,

                                              borderRadius:
                                              BorderRadius.all(Radius.circular(10))),
                                          // child: SlotsWidget()
                                        )
                                    );
                                  }
                              ),
                            ),


                            SizedBox(height: 20,),


                            Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Players',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Color(0xff41287B),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [

                                IconButton(
                                  onPressed: () {
                                    if (no_of_players > 0) {
                                      setState(() {
                                        no_of_players -= 1;
                                      });
                                    }
                                  },
                                  icon: Icon(
                                    Icons.remove,
                                    size: 20,
                                  ),
                                ),

                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  no_of_players.toString(),
                                  style: TextStyle(
                                    color: Color(0xffFF9E40),
                                    fontSize: 17,
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),

                                IconButton(
                                  onPressed: () {
                                    setState(() {
                                      no_of_players += 1;
                                    });
                                  },
                                  icon: Icon(
                                    Icons.add,
                                    size: 20,
                                  ),
                                ),

                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 100,
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 25,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    height: 70,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [

                        Container(
                          width: 125,
                          height: 35,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: MaterialButton(
                            onPressed: () async {
                              insertTime();
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BookPage2(
                                    selectedslots: time1.text,
                                    date: homepagectr.selectedDateTime.toString(),
                                    players: no_of_players,
                                    venue: widget.name,
                                    category: widget.category,
                                    contact: widget.contact,
                                    totalAmount: widget.rate,
                                  ),
                                ),
                              );
                            },
                            child: Center(
                              child: Text(
                                'BOOK NOW',
                                style: TextStyle(
                                  color: Color(
                                    0xff41287B,
                                  ),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
    )
              ]
            )
    );

  }
}

Card _listTileDateTime(
    {BuildContext context,
      String iconAsset,
      String title,
      String traling,
      Function onPressed}) {
  final size = MediaQuery.of(context).size;
  return Card(
    color: cAnccent,
    child: ListTile(
      contentPadding: EdgeInsets.only(left: 5,right: 5),
      trailing: roundedIconBox(
          icon: AssetsUtils.calendar,
          bgColor: Colors.green.withOpacity(0.1),
          iconColor: Colors.green.shade900,
          width: 40,
          height: 40),
      leading: Text(
        traling,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
        style: Get.textTheme.headline5.copyWith(color: cBlack, fontSize: 19),
      ),
      onTap: onPressed,
    ),
  );
}


