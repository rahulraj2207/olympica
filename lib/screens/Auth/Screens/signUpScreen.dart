import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/screens/Auth/Controllers/signUpController.dart';
import 'package:sportsgram/screens/Auth/widgets/myTextFiled.dart';

import 'package:bot_toast/bot_toast.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  Future<bool> _willPop() async {
    final signUpCtr = Get.find<SignUpController>();

    if (signUpCtr.isLoadimg) {
      return false;
    } else {
      return true;
    }
  }
@override
  void initState() {
  Firebase.initializeApp().whenComplete(() {
    print("completed");
    setState(() {});
  });    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    Get.put(SignUpController());
    return GestureDetector(
      onTap: () {
        if (Get.focusScope.hasFocus) {
          Get.focusScope.unfocus();
        }
      },
      child: WillPopScope(
        onWillPop: _willPop,
        child: Scaffold(
          appBar: buildAppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios_rounded),
              onPressed: () => Get.close(1),
            ),
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Register',style: TextStyle(
                  fontWeight: FontWeight.bold,fontSize: 30,
                ),),
                SizedBox(
                  height: Get.height * 0.05,
                ),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled(
                          enums: TextFieldSwitchers.sFirstName,
                          hintText: 'First name'),
                    ),
                    GetX<SignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.nameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.nameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled(
                        enums: TextFieldSwitchers.sSurname,
                        hintText: 'Surname',
                      ),
                    ),
                    GetX<SignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: 'surname',
                                // controller.surnameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.surnameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                // const PhoneNumberWidget(),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled(
                          enums: TextFieldSwitchers.sEmail,
                          hintText: 'Email address'),
                    ),
                    GetX<SignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.emailError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.emailError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled(
                          enums: TextFieldSwitchers.sUserName,
                          hintText: 'Username'),
                    ),
                    GetX<SignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.userNameError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.userNameError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                Row(
                  children: [
                    Expanded(
                      child: MyTextFiled(
                          enums: TextFieldSwitchers.sPassword,
                          hintText: 'Password'),
                    ),
                    GetX<SignUpController>(
                      builder: (controller) => buildErrorIcon(
                          onPressed: () {
                            BotToast.showText(
                                text: controller.passwordError.value,
                                align: const Alignment(0, 0));
                          },
                          errorMsg: controller.passwordError.value),
                    )
                  ],
                ),
                SizedBox(height: Get.height * 0.03),
                // const CheckBoxTC(),
                SizedBox(height: Get.height * 0.01),
                SizedBox(height: Get.height * 0.03),
                const SignUpButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SignUpButton extends StatelessWidget {
  const SignUpButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder
    <SignUpController>
      (
      builder: (ctr) => Align(
        alignment: Alignment.center,
        child: buildMyButton(
          onpressed:
          // (ctr.name == null ||
          //     ctr.surname == null ||
          //     ctr.myDob == null ||
          //     ctr.phone == null ||
          //     ctr.email == null ||
          //     ctr.username == null ||
          //     ctr.password == null)
          //     ? null
          //     :
              () {
            {
              Get.focusScope.unfocus();
              Get.find<SignUpController>().validateRahul();
            }
          },
          text: 'Continue',
          width: Get.width > 600 ? 550 : Get.width,
          height: 60,
        ),
      ),
    );
  }
}
