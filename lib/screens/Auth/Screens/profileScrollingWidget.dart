
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/signUpData.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sportsgram/screens/Auth/Screens/loginScreen.dart';
import 'package:sportsgram/utils/utils/assets/assetsUtils.dart';

String key;


class ProfileScrollingWidget extends StatefulWidget {

  ProfileScrollingWidget(
      {
        this.emaill,
      });
  String emaill;




  @override
  _ProfileScrollingWidgetState createState() => _ProfileScrollingWidgetState();
}

class _ProfileScrollingWidgetState extends State<ProfileScrollingWidget> {



  TextEditingController reviewcontroller = TextEditingController();
  void addReview() {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 250),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Material(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Container(
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 20, bottom: 13),
                    width: MediaQuery.of(context).size.width * .7,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Add Bio',
                          style: TextStyle(
                            color: Colors.green.shade900,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          width: double.maxFinite,
                          // height: 40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.green.shade900,
                            ),
                          ),
                          child: TextFormField(
                            maxLines: 10,
                            controller: reviewcontroller,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, right: 10, bottom: 5, top: 10),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: 110,
                              height: 35,
                              decoration: BoxDecoration(
                                color: Color(0xffFF9E40),
                                borderRadius: BorderRadius.circular(8),
                                border: Border.all(
                                  color: Color(0xffFF9E40),
                                ),
                              ),
                              child: MaterialButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text(
                                  'CANCEL',
                                  style: TextStyle(color: Colors.green.shade900),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: 110,
                              height: 35,
                              decoration: BoxDecoration(
                                color: Color(0xffFF9E40),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: MaterialButton(
                                onPressed: (){
                                  setState(() {

                                  });

                                  SignUpStore().update(widget.emaill, reviewcontroller.text,key);

                                  print(reviewcontroller.text);
                                  print("key");

                                  Get.back();

                                },
                                child: Text(
                                  'ADD',
                                  style: TextStyle(color: Colors.green.shade900),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }



  final ScrollController scrollController = ScrollController();


  var userList = List<SignUpData>().obs;


  void getUSER(String emm)async {
    if (await SignUpStore().database1(emm) != null) {

      for (var user in await SignUpStore().getFromDb1(emm)) {
        userList.add(user);
      }
    }
  }



  final ScrollController _scrollController = ScrollController();


  @override
  void initState() {
    getUSER(widget.emaill);
    print(userList.length);
    setState(() {

    });
    super.initState();
  }



  @override
  Widget build(BuildContext context) {


    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: SafeArea(
          child: Column(mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: size.height/2.5,
                width: size.width,
                child: Obx(
                      () {

                    if (userList.isEmpty)

                      return Center(child: CircularProgressIndicator(color: Colors.green,));
                    else
                      return ListView.builder(
                        controller: _scrollController,
                        padding: const EdgeInsets.only(bottom: 25, top: 15),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: userList.length,
                        itemBuilder: (context, i)
                        {

                          key = userList[i].key;


                          return Padding(
                            padding:   EdgeInsets.only(left: 20,right: 20,bottom: 25),

                            child: Profilecard(
                              username: userList[i].username,
                              name: userList[i].name,
                              surname: userList[i].surname,
                              emai: userList[i].email1,
                              bio: userList[i].bio,


                            ),

                          );


                        },
                      );
                  },
                ),
              ),



              SizedBox(
                height: size.height * 0.02,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SmallButtons(
                    isBordered: true,
                    text: "Add Bio",
                    onPressed: () {
                      addReview();
                    },
                    bgColor: cWhite,
                    fontColor: cBlack,
                  ),
                ],
              ),
              SizedBox(
                height: size.height * 0.02,
              ),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: ListTile(
                  contentPadding: EdgeInsets.only(left: 5,right: 5),
                  trailing: roundedIconBox(
                      icon: AssetsUtils.logout,
                      bgColor: Colors.green.withOpacity(0.2),
                      iconColor: Colors.green.shade900,
                      width: 40,
                      height: 40),
                  leading: Text(
                    "Log out",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Get.textTheme.headline5.copyWith(color: cBlack.withOpacity(.5), fontSize: 22,fontWeight: FontWeight.w500),
                  ),
                  onTap: ()async{

                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.remove('islogged');
                    Get.to(LoginScreen());


                  },
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }

  Expanded buildFollowCounts(
      {@required String counts, @required String text, Function onTap}) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        behavior: HitTestBehavior.opaque,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            buildheadings(
                text: counts, height: 24, alignment: Alignment.center),
            buildCaptions(text: text, alignment: Alignment.center),
          ],
        ),
      ),
    );
  }
}

class ExpandableRow extends StatelessWidget {
  final Function onPressed;
  final String heading;
  final String trailing;
  final EdgeInsetsGeometry padding;

  const ExpandableRow(
      {Key key, this.onPressed, this.heading, this.trailing, this.padding})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: padding ??
          EdgeInsets.symmetric(
              vertical: size.height * 0.03, horizontal: size.width * 0.03),
      child: GestureDetector(
        onTap: onPressed,
        behavior: HitTestBehavior.opaque,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              heading,
              style: Get.textTheme.subtitle1
                  .copyWith(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            Text(
              trailing,
              style: Get.textTheme.headline6,
            ),
          ],
        ),
      ),
    );
  }
}

class SmallButtons extends StatelessWidget {
  SmallButtons({
    @required this.text,
    this.fontColor,
    this.bgColor,
    @required this.onPressed,
    @required this.isBordered,
  });
  final String text;
  final Color fontColor;
  final Color bgColor;
  final Function onPressed;
  final bool isBordered;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 30,
        width: Get.width / 4,
        decoration: BoxDecoration(
            color: bgColor ?? cPrimary,
            borderRadius: BorderRadius.circular(10),
            border: !isBordered
                ? null
                : Border.all(
              color: cDarkGrey.withOpacity(0.5),
              width: 1.5,
            )),
        alignment: Alignment.center,
        child: Text(
          text,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: TextStyle(fontSize: 14),
        ),
      ),
    );
  }
}




class Profilecard extends StatelessWidget {
  final String username;
  final String name;
  final String surname;
  final String emai;
  final String bio;

  const Profilecard(
      {Key key,
        this.username,
        this.bio,
        this.name,
        this.surname,
        this.emai})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SizedBox(
      height: 300 ,
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Divider(thickness: 1),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Text(
                      username??"",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black.withOpacity(.6),
                        fontSize: 25,
                      )
                  ),
                ),
              ],
            ),
            Divider(thickness: 1),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                Text(
                  name ??"",
                  textAlign: TextAlign.left,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black.withOpacity(.6),
                  ),
                ),
                SizedBox(width: 5),
                Text(
                  surname??"",
                  textAlign: TextAlign.left,
                  style: Get.textTheme.subtitle1.copyWith(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                    color: Colors.black.withOpacity(.6),
                  ),
                ),

              ],
            ),
            Divider(thickness: 1),


            Center(
              child: Text(
                emai??"",
                textAlign: TextAlign.left,
                style: Get.textTheme.subtitle1.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  color: Colors.black.withOpacity(.6),
                ),
              ),
            ),
            Divider(thickness: 1),

            SizedBox(height: 20,),

            Center(
              child: Container(
                width: size.width/1.3,
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.green.shade900,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Center(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Text(
                          bio??"Bio",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black.withOpacity(.6),
                            fontSize: 20,
                          )
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}



