import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:sportsgram/screens/Auth/Screens/joinMatch.dart';
import 'package:sportsgram/utils/utils/theme.dart';

File a;
String _downloadUrl;

class AllMatches extends StatefulWidget {


  @override
  _AllMatchesState createState() => _AllMatchesState();
}


class _AllMatchesState extends State<AllMatches> {


  var matchList = List<Match>().obs;

  void getMatchList()async {
    if (await MatchStore().database() != null) {

      for (var matches in await MatchStore().getFromDb()) {
        matchList.add(matches);
      }
    }
  }

  @override
  void initState() {

    getMatchList();
    setState(() {

    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {


    final ScrollController _scrollController = ScrollController();



    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      controller:Get.find<HomePageController>().searchScrollController,
      child: Column(
          children: [
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.only(right: 15,bottom: 10),
              child: Container(
                height: size.height/1.3,
                width: size.width/1.03,
                child: Obx(
                      () {

                    if (matchList.isEmpty)
                      return Center(child: CircularProgressIndicator(color: Colors.green,));
                    else
                      return ListView.builder(
                        controller: _scrollController,
                        padding: const EdgeInsets.only(bottom: 25, top: 15),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: matchList.length,
                        itemBuilder: (context, i)
                        {
                          List da=  matchList[i].mdate.split(' ');
                          String d = da[0];

                          print("insidelisvw");
                          return Padding(
                            padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15,bottom: 25),

                            child: EventsCard1(
                              eventHeaderImage: "https://www.bls.gov/spotlight/2017/sports-and-exercise/images/cover_image.jpg",
                              profileUserImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyqgVNWBJBPqsTl-3tfAsaBtcoTBsoIYv9Qw&usqp=CAU",
                              matchName: matchList[i].mname,
                              hname: matchList[i].hname,
                              time: matchList[i].mtime,
                              date: d,
                              groundName: matchList[i].mground,
                              category: matchList[i].mcategory,
                              eventRating: 1,
                              joinB: matchList[i].join,
                              key1: matchList[i].key,
                              onpressed: (){
                                setState(() {
                                  print("hi");

                                });
                              },


                            ),
                          );
                        },
                      );
                  },
                ),
              ),
            ),

          ]),
    );
  }
}


class EventsCard1 extends StatefulWidget {
  final String eventHeaderImage;
  final String profileUserImage;
  final String hname;
  final String date;
  final String time;
  final String groundName;
  final int eventRating;
  final String matchName;
  final String location;
  final bool joinB;
  final String key1;
  final String category;

  final Function() onpressed;


  const EventsCard1(
      {
        this.key1,
        this.category,
        this.date,
        this.onpressed,
        this.time,
        this.location,
        this.matchName,
        this.eventHeaderImage,
        this.profileUserImage,
        this.hname,
        this.eventRating,
        this.joinB,
        this.groundName});

  @override
  _EventsCard1State createState() => _EventsCard1State();
}

class _EventsCard1State extends State<EventsCard1> {


  @override
  void initState() {
    setState(() {

    });
    super.initState();
  }





  @override
  Widget build(BuildContext context) {


    var height = 330;
    return SizedBox(
        height: height.toDouble(),
        width: 300,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.2),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (widget.eventHeaderImage != null)
                        Image.network(
                          widget.eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),
                      // Align(
                      //   alignment: Alignment.topRight,
                      //   child: Padding(
                      //     padding: const EdgeInsets.all(8.0),
                      //     child: SizedBox(
                      //         height: 25,
                      //         child: DecoratedBox(
                      //           decoration: BoxDecoration(
                      //               color: cWhite,
                      //               borderRadius: BorderRadius.circular(25)),
                      //           child: Padding(
                      //             padding: const EdgeInsets.symmetric(
                      //                 horizontal: 10),
                      //             child: Row(
                      //               mainAxisSize: MainAxisSize.min,
                      //               children: [
                      //                 Icon(
                      //                   Icons.location_on,
                      //                   size: 16,
                      //                   color: cPrimary,
                      //                 ),
                      //                 const Text('1.9 km')
                      //               ],
                      //             ),
                      //           ),
                      //         )),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),


              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(children: [
                          CircleAvatar(
                            backgroundImage: widget.profileUserImage == null
                                ? null
                                : NetworkImage(widget.profileUserImage),
                            backgroundColor: clightGrey,
                            radius: 15,
                          ),
                          const SizedBox(width: 4),
                          Text(widget.hname??"",style: TextStyle(
                              fontSize: 16,fontWeight: FontWeight.w600
                          ),)
                        ],),

                        Text(widget.category??"",style: TextStyle(
                            fontWeight: FontWeight.bold,fontSize: 17,color: Colors.red
                        ),)


                      ],
                    ),
                    Divider(thickness: 1,),


                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Align(alignment: Alignment.topLeft,
                          child: Text(
                            widget.matchName??"",
                            textAlign: TextAlign.left,
                            style: Get.textTheme.subtitle1.copyWith(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                        // SizedBox(width: size.width/5),
                        Row(children: [

                          Text(
                            widget.date??"",
                            textAlign: TextAlign.left,
                            style: Get.textTheme.subtitle1.copyWith(
                              fontWeight: FontWeight.w500,
                              fontSize: 15,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          SizedBox(width: 10),


                          Icon(
                            Icons.date_range,
                            size: 18,
                            color: Colors.green,
                          ),



                        ],),



                      ],
                    ),


                    Divider(thickness: 1,),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Icon(CupertinoIcons.location,color: Colors.red,),
                            SizedBox(width: 7,),
                            Text(
                              widget.groundName??"",
                              textAlign: TextAlign.left,
                              style: Get.textTheme.subtitle1.copyWith(
                                color: Colors.green.shade700.withOpacity(.8),
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),

                          ],
                        ),

Row(
  children: [
    Text(
      widget.time??"",
      textAlign: TextAlign.left,
      style: Get.textTheme.subtitle1.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: 15,
      ),
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
    ),
    SizedBox(width: 10),
    Icon(CupertinoIcons.time,color: Colors.green,size: 18,),



  ],
),

                      ],
                    ),
                    Divider(thickness: 1,),

                    SizedBox(height: 5),

                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SvgPicture.asset('assets/svg/user-check.svg',color: Colors.green),
                        const SizedBox(
                          width: 8,
                        ),

                        Align(
                          alignment: Alignment.bottomRight,
                          child: SizedBox(height: 27,width: 100,
                            child:
                            TextButton(onPressed: widget.joinB?(){


                            }:(){
                              setState(() {

                                print(widget.joinB);

                              });
                              Get.to(JoinMatch(keyy: widget.key1,));
                            },
                                style: TextButton.styleFrom(
                                    backgroundColor:  widget.joinB?Colors.red:Colors.green,
                                    primary: Colors.white,

                                    side: BorderSide(color: Colors.green,width: 0.01,)
                                ),
                                child: Text(widget.joinB?
                                'Filled':'Join',style: TextStyle(fontSize: 12,fontWeight: FontWeight.w900),
                                )),
                          ),
                        )

                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}


