import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils//widgets.dart';


File a;
String _downloadUrl;



class YourMatches extends StatefulWidget {

  YourMatches({

    this.email,

  });

  String email;



  @override
  _YourMatchesState createState() => _YourMatchesState();
}


class _YourMatchesState extends State<YourMatches> {


  var userMatchlist = List<Match>().obs;

  void getuserMatchList(String em)async {
    if (await MatchStore().databaseInuser(em) != null) {

      for (var match in await MatchStore().getFromDBInuser(em)) {
        userMatchlist.add(match);
      }
    }
  }


  @override
  void initState() {
    getuserMatchList(widget.email);
    setState(() {

    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {


    final ScrollController _scrollController1 = ScrollController();



    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      controller:Get.find<HomePageController>().searchScrollController,
      child: Column(
          children: [

            Container(
              height: size.height/1.3,
              width: size.width/1.03,
              child: Obx(
                    () {
                  print("insideobx");
                  print(userMatchlist.length);
                  if (userMatchlist.isEmpty)

                    return Center(child: CircularProgressIndicator(color: Colors.green,));
                  else
                    return ListView.builder(
                      controller: _scrollController1,
                      padding:  EdgeInsets.only(bottom: 50, top: 15,right: 15),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      // physics: const NeverScrollableScrollPhysics(),
                      itemCount: userMatchlist.length,
                      itemBuilder: (context, i)
                      {

                        List daa=  userMatchlist[i].mdate.split(' ');
                        String d1 = daa[0];
                        print("insidelisvw2222");
                        return Padding(
                          padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15,bottom: 25),

                          child: EventsCard2(
                            eventHeaderImage: "https://www.bls.gov/spotlight/2017/sports-and-exercise/images/cover_image.jpg",
                            profileUserImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyqgVNWBJBPqsTl-3tfAsaBtcoTBsoIYv9Qw&usqp=CAU",
                            matchName: userMatchlist[i].mname,
                            hname: userMatchlist[i].hname,
                            time: userMatchlist[i].mtime,
                            date: d1,
                            groundName: userMatchlist[i].mground,
                            eventRating: 1,
                            category: userMatchlist[i].mcategory,
                            key1: userMatchlist[i].key,
                          ),
                        );
                      },
                    );
                },
              ),
            ),


          ]),
    );
  }
}


class EventsCard2 extends StatefulWidget {
  final String eventHeaderImage;
  final String profileUserImage;
  final String hname;
  final String date;
  final String time;
  final String groundName;
  final int eventRating;
  final String matchName;
  final String key1;
  final String category;


  const EventsCard2(
      {
        this.key1,
        this.date,
        this.time,
        this.category,
        this.matchName,
        this.eventHeaderImage,
        this.profileUserImage,
        this.hname,
        this.eventRating,
        this.groundName});

  @override
  _EventsCard2State createState() => _EventsCard2State();
}

class _EventsCard2State extends State<EventsCard2> {


  @override
  void initState() {
    setState(() {

    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    var height = 250;
    return SizedBox(
        height: height.toDouble(),
        width: 300,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.2),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (widget.eventHeaderImage != null)
                        Image.network(
                          widget.eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),
                    ],
                  ),
                ),
              ),


              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: widget.profileUserImage == null
                                  ? null
                                  : NetworkImage(widget.profileUserImage),
                              backgroundColor: clightGrey,
                              radius: 15,
                            ),
                            const SizedBox(width: 5),
                            Text(widget.hname??"",style: TextStyle(
                                fontWeight: FontWeight.bold,fontSize: 17
                            ),),
                          ],
                        ),

                        Text(widget.category??"",style: TextStyle(
                            fontWeight: FontWeight.bold,fontSize: 17,color: Colors.red
                        ),)


                      ],
                    ),
                    Divider(thickness: 1,),


                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Align(alignment: Alignment.topLeft,
                          child: Text(
                            widget.matchName??"",
                            textAlign: TextAlign.left,
                            style: Get.textTheme.subtitle1.copyWith(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                        // SizedBox(width: size.width/5),

Row(
  children: [
    Text(
      widget.date??"",
      textAlign: TextAlign.left,
      style: Get.textTheme.subtitle1.copyWith(
        fontWeight: FontWeight.w500,
        fontSize: 15,
      ),
      overflow: TextOverflow.ellipsis,
      maxLines: 2,
    ),
    SizedBox(width: 10),
    Icon(
      Icons.date_range,
      size: 18,
      color: Colors.green,
    ),


  ],
),


                      ],
                    ),


                    Divider(thickness: 1,),
                    Row(
                      children: [
                     Icon(CupertinoIcons.location,color: Colors.red,),
                        SizedBox(width: 7,),
                        Text(
                          widget.groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            color: Colors.green.shade700.withOpacity(.8),
                            fontWeight: FontWeight.w700,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ],
                    ),

                  ],
                ),
              )
            ],
          ),
        ));
  }
}


