import 'dart:math';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/screens/Auth/Controllers/getGroundListController.dart';
import 'package:sportsgram/screens/Auth/widgets/ratingStar.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/screens/Auth/widgets/searchTextfield.dart';
import 'package:sportsgram/screens/Auth/Screens/bookpage1.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'dart:math';
int max = 4;



const colorizeColors = [
  Colors.white,
  Colors.purple,
  Colors.blue,
  Colors.yellow,
  Colors.red,
];

const colorizeTextStyle = TextStyle(
  fontSize: 25.0,fontWeight: FontWeight.bold
);
List<Ground> groundList = List<Ground>().obs;

class  SearchScreen extends StatefulWidget {


  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {


  final GetListControllerU getListController = Get.find();

  Position _currentPosition;

  double userCurrentLocationLat;
  double userCurrentLocationLong;

  _getCurrentLocation()async {

    Position pos =await _determinePosition();

    setState(() {

      _currentPosition = pos;
      userCurrentLocationLat = pos.latitude;
      userCurrentLocationLong = pos.longitude;
      getUserFromLatLng();

    });

  }

  bool locenable = false;


  Future<Position> _determinePosition() async {
    LocationPermission permission;
    bool serviceEnabled;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    locenable = serviceEnabled;
    if (!serviceEnabled) {

      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {

        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }

  String userLocation;
  List loclist;
  String userLocation2;



  getUserFromLatLng() async {
    print("latlon");

    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentPosition.latitude,
          _currentPosition.longitude
      );

      Placemark place = placemarks[0];

      setState(() {

        userLocation = "${place.name}";
        loclist = userLocation.split(" ");
        userLocation2= loclist[0];


      });
    } catch (e) {
      print(e);
    }
  }





  double calculateDistance (double lat1,double lng1,double lat2,double lng2){
    double radEarth =6.3781*( pow(10.0,6.0));
    double phi1= lat1 * (pi/180);
    double phi2 = lat2 * (pi/180);

    double delta1=(lat2-lat1)*(pi/180);
    double delta2=(lng2-lng1)*(pi/180);

    double cal1 = sin(delta1/2)*sin(delta1/2)+(cos(phi1)*cos(phi2)*sin(delta2/2)*sin(delta2/2));

    double cal2= 2 * atan2((sqrt(cal1)), (sqrt(1-cal1)));
    double distance =(radEarth*cal2)/1000;


    return distance;

  }


  List distanceList; //list defination





  List<String> searcEventsButtons = intrestCategories;
  @override
  void initState() {
    _getCurrentLocation();
    // getUserFromLatLng();

setState(() {

});


    super.initState();
    print("p");
    print(getListController.groundList.length);

  }




  final ScrollController _scrollController = ScrollController();


  @override
  Widget build(BuildContext context) {


    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            Get.focusScope.unfocus();
          },
          child: SingleChildScrollView(

            controller: Get.find<HomePageController>().searchScrollController,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: size.width * 0.04,
                              vertical: size.height * 0.03),
                          child: SizedBox(
                            width: 250.0,
                            child: AnimatedTextKit(
                              animatedTexts: [
                                ColorizeAnimatedText(
                                  'Olympica',
                                  textStyle: colorizeTextStyle,
                                  colors: colorizeColors,
                                ),
                              ],
                              isRepeatingAnimation: true,

                            ),
                          )
                      ),

                      Column(
                        children: [
                          userLocation2==null?
                          GestureDetector(
                            onTap: (){
                              _getCurrentLocation();

                            },
                            child: Icon(
                              Icons.location_off,
                              size: 20,
                              color: Colors.red,
                            ),
                          ):Icon(
                            Icons.location_on,
                            size: 20,
                            color: Colors.green,
                          ),
                          SizedBox(height: 3,),
                          Align(alignment: Alignment.topRight,
                            child: Text(userLocation2??"enable location",style: TextStyle(
                                fontSize: 12,fontWeight: FontWeight.bold,color: Colors.green
                            ),),
                          ),

                        ],
                      ),


                    ],
                  ),
                ),




                Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.04),
                  child: DecoratedBox(
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(12)),
                      child: SearchTextField(
                        onChangedDataBack: (va) {},
                      )),
                ),
                SizedBox(
                  height: size.height * 0.03,
                ),




                buildHedingWithSub(
                  context: context,
                  padding: EdgeInsets.symmetric(horizontal: size.width * 0.04),
                  title: 'Grounds for you',style: TextStyle(fontSize: 25,fontWeight: FontWeight.w500),
                  onpressed: () {

                  },
                ),
                Container(
                  height: 352,
                  width: size.width/1.03,
                  child: Obx(
                        () {
                          print("insideobxsearch");
                          print(getListController.groundList.length);
                          if (getListController.groundList.isEmpty)

                        return Center(child: CircularProgressIndicator());
                      else
                        return ListView.builder(
                      controller: _scrollController,
                       padding: const EdgeInsets.only(bottom: 25, top: 15),
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          // physics: const NeverScrollableScrollPhysics(),
                          itemCount: getListController.groundList.length,
                          itemBuilder: (context, i)
                          {
                            int randomNumber = Random().nextInt(max) + 1;
                            double distance;
                            if(locenable) {
                               distance = calculateDistance(
                                  userCurrentLocationLat,
                                  userCurrentLocationLong,
                                  getListController.groundList[i].lat,
                                  getListController.groundList[i].long);
                            }else{
                              distance = 1.22;

                            }

                            print("insidelisvw");
                            return Padding(
                                     padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15),

                              child: GestureDetector(
                                onTap: (){
                                  //
                                  Get.to(BookPage1(name: getListController.groundList[i].gname,rate: getListController.groundList[i].open,
                                    category: getListController.groundList[i].gabout,contact: getListController.groundList[i].gdetails,));
                                },
                                child: EventsCard(
                                  distance: distance,
                                  eventHeaderImage: getListController.groundList[i].gimage,
                                  about: getListController.groundList[i].gabout,
                                  details: getListController.groundList[i].gdetails,
                                  groundName: getListController.groundList[i].gname,
                                  rate: getListController.groundList[i].open,
                                  eventRating: randomNumber,

                                ),
                              ),
                            );


                          },
                        );
                    },
                  ),
                ),

                SizedBox(
                  height: 20,
                  width: size.width,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  OutlineButton buildOutlineButtons({
    Function onPressed,
    @required String text,
  }) {
    return OutlineButton(
      onPressed: onPressed,
      highlightedBorderColor: Colors.black,
      child: Text(
        text,
        style: Get.textTheme.subtitle1,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}


class EventsCard extends StatelessWidget {
  final String eventHeaderImage;
  final String about;
  final String details;
  final String groundName;
  final int eventRating;
  final double distance;
  final String rate;

  const EventsCard(
      {Key key,
        this.distance,
        this.eventHeaderImage,
        this.about,
        this.rate,
        this.details,
        this.eventRating,
        this.groundName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = 312;
    return SizedBox(
        height: height.toDouble(),
        width: 230,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.2),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (eventHeaderImage != null)
                        Image.network(
                          eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                              height: 25,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                    color: cWhite,
                                    borderRadius: BorderRadius.circular(25)),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        size: 16,
                                        color: Colors.red,
                                      ),
                                       Text(distance.toStringAsFixed(2).toString()??""),
                                      Text(" KM"),

                                    ],
                                  ),
                                ),
                              )),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(children: [

                              Text(
                                about??"",
                                textAlign: TextAlign.left,
                                style: Get.textTheme.subtitle1.copyWith(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),


                            ],),

                            Row(children: [

                              Text(
                                "₹",
                                textAlign: TextAlign.left,
                                style: Get.textTheme.subtitle1.copyWith(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,color: Colors.red
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              Text(
                                rate??"",
                                textAlign: TextAlign.left,
                                style: Get.textTheme.subtitle1.copyWith(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,color: Colors.red
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ), Text(
                                " /hr",
                                textAlign: TextAlign.left,
                                style: Get.textTheme.subtitle1.copyWith(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,color: Colors.red
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),

                            ],),
                          ],
                        ),
                        Text(
                          groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),

                        Row(children: [

                          Icon(CupertinoIcons.phone,size: 20,),
SizedBox(width: 5,),

                          Text(
                            details??"",
                            textAlign: TextAlign.left,
                            style: Get.textTheme.subtitle1.copyWith(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),

                        ],),






                        Row(mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Rating",style: TextStyle(
                                fontSize: 15
                            ),
                            ),

                            Expanded(
                              child: RatingWithStar(
                                rating: eventRating ?? 0,
                              ),
                            ),

                          ],
                        ),

                        // )
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
