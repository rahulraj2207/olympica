import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sportsgram/controller/homePageController.dart';
import 'package:sportsgram/firebase/createEventData.dart';
import 'package:sportsgram/screens/Auth/Screens/joinMatch.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils//widgets.dart';
import 'package:firebase_storage/firebase_storage.dart';


File a;
String _downloadUrl;


  // Future downloadImage()async{
  //   Reference _reference=FirebaseStorage.instance.ref().child("_uploadedFileURL/$a.png");
  //   String downloadAddress=await _reference.getDownloadURL();
  //   setState(() {
  //     _downloadUrl=downloadAddress;
  //   });
  // }


class EventsBody extends StatefulWidget {


  EventsBody({

    this.email,

  });

  String email;



  @override
  _EventsBodyState createState() => _EventsBodyState();
}


class _EventsBodyState extends State<EventsBody> {


  var matchList = List<Match>().obs;



  void getMatchList()async {
    if (await MatchStore().database() != null) {

      for (var matches in await MatchStore().getFromDb()) {
        matchList.add(matches);
      }
    }
  }



  var userMatchlist = List<Match>().obs;


  void getuserMatchList(String em)async {
    if (await MatchStore().databaseInuser(em) != null) {

      for (var match in await MatchStore().getFromDBInuser(em)) {
        userMatchlist.add(match);
      }
    }
  }




  @override
  void initState() {

    getMatchList();
    getuserMatchList(widget.email);
setState(() {

});
    super.initState();
  }


  @override
  Widget build(BuildContext context) {


    final ScrollController _scrollController1 = ScrollController();

    final ScrollController _scrollController2 = ScrollController();


    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      controller:Get.find<HomePageController>().searchScrollController,
      child: Column(
          children: [
            SizedBox(height: 10,),
            Padding(
          padding: const EdgeInsets.only(right: 15,bottom: 10),
          child: Container(
            height: 400,
            width: size.width/1.03,
            child: Obx(
                  () {

                    if (matchList.isEmpty)
                  return Center(child: CircularProgressIndicator());
                else
                  return ListView.builder(
                    controller: _scrollController1,
                    padding: const EdgeInsets.only(bottom: 25, top: 15),
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: matchList.length,
                    itemBuilder: (context, i)
                    {


                     List da=  matchList[i].mdate.split(' ');
                     String d = da[0];

                      print("insidelisvw");
                      return Padding(
                        padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15,bottom: 25),

                        child: EventsCard1(
                          eventHeaderImage: "https://www.bls.gov/spotlight/2017/sports-and-exercise/images/cover_image.jpg",
                           profileUserImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyqgVNWBJBPqsTl-3tfAsaBtcoTBsoIYv9Qw&usqp=CAU",
                          matchName: matchList[i].mname,
                          hname: matchList[i].hname,
                          time: matchList[i].mtime,
                          date: d,
                          location: matchList[i].mnum.toString(),
                          groundName: matchList[i].mground,
                          eventRating: 1,
                          joinB: matchList[i].join,
                          key1: matchList[i].key,
                       onpressed: (){
                            setState(() {
                              print("hi");

                            });
                       },


                        ),
                      );
                    },
                  );
              },
            ),
          ),
        ),
            Divider(thickness: 3,),


            Text("your matches"),


            Container(
              height: 410,
              width: size.width/1.03,
              child: Obx(
                    () {
                  print("insideobx");
                  print(userMatchlist.length);
                  if (userMatchlist.isEmpty)

                    return Center(child: CircularProgressIndicator());
                  else
                    return ListView.builder(
                      controller: _scrollController2,
                      padding: const EdgeInsets.only(bottom: 25, top: 15),
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      // physics: const NeverScrollableScrollPhysics(),
                      itemCount: userMatchlist.length,
                      itemBuilder: (context, i)
                      {

                        List daa=  userMatchlist[i].mdate.split(' ');
                        String d1 = daa[0];


                        print("insidelisvw2222");
                        return Padding(
                          padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15),

                          child: EventsCard1(
                            eventHeaderImage: "https://www.bls.gov/spotlight/2017/sports-and-exercise/images/cover_image.jpg",
                            profileUserImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyqgVNWBJBPqsTl-3tfAsaBtcoTBsoIYv9Qw&usqp=CAU",
                            matchName: userMatchlist[i].mname,
                            hname: userMatchlist[i].hname,
                            time: userMatchlist[i].mtime,
                            date: d1,
                            location: userMatchlist[i].mnum.toString(),
                            groundName: userMatchlist[i].mground,
                            eventRating: 1,
                            joinB: userMatchlist[i].join,
                            key1: userMatchlist[i].key,
                            onpressed: (){
                              setState(() {
                                print("hi");

                              });
                            },


                          ),
                        );


                      },
                    );
                },
              ),
            ),









          ]),
    );
  }
}


class EventsCard1 extends StatefulWidget {
  final String eventHeaderImage;
  final String profileUserImage;
  final String hname;
  final String date;
  final String time;
  final String groundName;
  final int eventRating;
  final String matchName;
  final String location;
  final bool joinB;
  final String key1;

  final Function() onpressed;


  const EventsCard1(
      {
        this.key1,
        this.date,
        this.onpressed,
        this.time,
        this.location,
        this.matchName,
        this.eventHeaderImage,
        this.profileUserImage,
        this.hname,
        this.eventRating,
        this.joinB,
        this.groundName});

  @override
  _EventsCard1State createState() => _EventsCard1State();
}

class _EventsCard1State extends State<EventsCard1> {


  @override
  void initState() {
    setState(() {

    });
    super.initState();
  }





  @override
  Widget build(BuildContext context) {


    var height = 320;
    return SizedBox(
        height: height.toDouble(),
        width: 300,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.2),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (widget.eventHeaderImage != null)
                        Image.network(
                          widget.eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                              height: 25,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                    color: cWhite,
                                    borderRadius: BorderRadius.circular(25)),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        size: 16,
                                        color: cPrimary,
                                      ),
                                      const Text('1.9 km')
                                    ],
                                  ),
                                ),
                              )),
                        ),
                      )
                    ],
                  ),
                ),
              ),


              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          backgroundImage: widget.profileUserImage == null
                              ? null
                              : NetworkImage(widget.profileUserImage),
                          backgroundColor: clightGrey,
                          radius: 15,
                        ),
                        const SizedBox(width: 4),
                        buildCaptions(
                            text:widget.hname??"",
                            alignment: Alignment.centerLeft),


                        // attendButton(),


                      ],
                    ),
                    Divider(thickness: 1,),


                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [


                        Align(alignment: Alignment.topLeft,
                          child: Text(
                            widget.matchName??"",
                            textAlign: TextAlign.left,
                            style: Get.textTheme.subtitle1.copyWith(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                        // SizedBox(width: size.width/5),


                        Text(
                          widget.date??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),

                        Text(
                          widget.time??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        Icon(
                          Icons.date_range,
                          size: 18,
                          color: Colors.green,
                        ),


                      ],
                    ),


                    Divider(thickness: 1,),
                    Row(
                      children: [
                        Text("@ ",style: TextStyle(
                          color: Colors.green,fontSize: 18,
                        ),),
                        SizedBox(width: 7,),
                        Text(
                          widget.groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            color: Colors.green.shade700.withOpacity(.8),
                            fontWeight: FontWeight.w700,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        Text(","),
                        Text(
                          widget.location??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            color: Colors.green,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),


                      ],
                    ),
                    Divider(thickness: 1,),

SizedBox(height: 5),

                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SvgPicture.asset('assets/svg/user-check.svg',color: Colors.green),
                        const SizedBox(
                          width: 8,
                        ),

      Align(
        alignment: Alignment.bottomRight,
        child: SizedBox(height: 27,width: 100,
          child:
          TextButton(onPressed: widget.joinB?(){


            }:(){
            setState(() {

              print(widget.joinB);

            });
            Get.to(JoinMatch(keyy: widget.key1,));
          },
              style: TextButton.styleFrom(
              backgroundColor:  widget.joinB?Colors.red:Colors.green,
              primary: Colors.white,

              side: BorderSide(color: Colors.green,width: 0.01,)
              ),
              child: Text(widget.joinB?
                'Filled':'Join',style: TextStyle(fontSize: 12,fontWeight: FontWeight.w900),
              )),
        ),
      )

                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}


