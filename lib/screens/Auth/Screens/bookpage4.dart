// import 'package:athlify_sports/Api/api.dart';
// import 'package:athlify_sports/Model/bookingDetailsModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sportsgram/screens/Auth/Screens/HomeScreen.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/screens/Auth/Screens/searchScreen.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:get/get.dart';

class BookPage4 extends StatefulWidget {


  BookPage4(
      {
        this.venue,
        this.date,
        this.selectedslots,
        this.players,
        this.category,
        this.contact,
        this.total
      });
  String venue;
  String date;
  String selectedslots;
  int players;
  int total;
  String category;
  String contact;



  @override
  _BookPage4State createState() => _BookPage4State();
}

class _BookPage4State extends State<BookPage4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
            body: SingleChildScrollView(
              child: Container(
                color:Colors.grey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 70,
                    ),
                    Text(
                      'READY TO PLAY!',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Stack(
                      children: [
                        Container(
                          height: 400,
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(horizontal: 25),
                          padding: EdgeInsets.symmetric(
                              horizontal: 25, vertical: 25),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(widget.venue??"",
                                style: TextStyle(
                                  color: Color(0xffFF9E40),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: 10,),
                              Text(widget.category??"",
                                style: TextStyle(
                                  color: Color(0xffFF9E40),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: 10,),

                              Text(widget.date??"",
                                style: TextStyle(
                                  color: Color(0xffFF9E40),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: 10,),

                              Text(
                                widget.selectedslots??"",
                                style: TextStyle(
                                  color: Color(0xffFF9E40),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Advance Paid ',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                  Text(
                                    '₹Advance',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Balance Amount',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                  Text(
                                    '₹ Balance',
                                        // '${(int.parse(snapshot.data!.totalPrice) - snapshot.data!.bookingData.advancePayment).toString()}',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Total Amount',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                  Text("₹"+widget.total.toString(),
                                    // '₹ ' + snapshot.data!.totalPrice ?? '',
                                    style: TextStyle(
                                      color: Color(0xffFF9E40),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            children: [
                              SizedBox(
                                height: 190,
                              ),
                              CircleAvatar(
                                backgroundColor: Colors.grey,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              SizedBox(
                                height: 190,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CircleAvatar(
                                    backgroundColor: Colors.grey,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: 25),
                      padding:
                      EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.venue??"",
                            // snapshot.data!.turfDetails.turfName ?? '',
                            style: TextStyle(
                                color: Color(0xff41287B),
                                fontSize: 15,
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.location_on,
                                size: 23,
                                color: Color(0xff41287B),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                "Kochi",
                                // snapshot.data!.turfDetails.location ?? '',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.phone,
                                size: 23,
                                color: Color(0xff41287B),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                widget.contact??"",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          );

  }
}
