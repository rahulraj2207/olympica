import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Screens/editProfile.dart';
import 'package:sportsgram/screens/Auth/Controllers/profileController.dart';
import 'package:sportsgram/screens/Auth/Screens/profileScrollingWidget.dart';
import 'package:image_picker/image_picker.dart';


File _image;
PickedFile _image1;
String _uploadedFileURL;

class ProfileScreen extends StatefulWidget {

  ProfileScreen(
      {
        this.userEmail,
       });
  String userEmail;




  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {




  @override
  void initState() {
    super.initState();
    Get.put(ProfileController());
  }

  @override
  void dispose() {
    super.dispose();
    Get.delete<ProfileController>();
  }

  @override
  Widget build(BuildContext context) {
    final profileCtr = Get.find<ProfileController>();
    profileCtr.heightBuilder();
    return SingleChildScrollView(
      child: Column(
        children: [


          SizedBox(height: 50),

          Picker(),

          ProfileScrollingWidget(emaill:widget.userEmail ),
        ],
      ),
    );
  }
}



class Picker extends StatefulWidget {
  @override
  _PickerState createState() => _PickerState();
}

class _PickerState extends State<Picker> {




  void _imgFromCamera()  async{
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera ,
    );
    setState(() {

      _image = File(pickedFile.path);


      // _image1 = _image;
    });
    Navigator.pop(context);
  }

  void _imgFromGallery() async{
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery ,
    );
    setState(() {
      _image = File(pickedFile.path);


      // _image = _image1;
    });

    Navigator.pop(context);
  }


  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Center(
        child: GestureDetector(
          onTap: () {
            _showPicker(context);
          },
          child: CircleAvatar(
            radius: 55,
            backgroundColor: Color(0xffFDCF09),
            child: _image != null
                ? ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.file(
                _image,
                width: 100,
                height: 100,
                fit: BoxFit.fitHeight,
              ),
            )
                : Container(
              decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(50)),
              width: 100,
              height: 100,
              child: Icon(
                Icons.camera_alt,
                color: Colors.grey[800],
              ),
            ),
          ),
        ),
      )
    );
  }
}

