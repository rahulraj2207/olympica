
import 'package:get/get.dart';
import 'package:sportsgram/screens/Auth/Controllers/loginController.dart';



class MainBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}
