import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/admin/adminHome.dart';

class AdminLogin extends StatefulWidget {
  @override
  _AdminLoginState createState() => _AdminLoginState();
}

class _AdminLoginState extends State<AdminLogin> {

  final TextEditingController usrctr = TextEditingController();
  final TextEditingController passctr = TextEditingController();



  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [


            Text("ADMIN",style: TextStyle(
              color: Colors.red,fontWeight: FontWeight.bold,fontSize: 25
            ),),
            SizedBox(height: 40),

            TextField(
              controller: usrctr,
              decoration: InputDecoration(
                filled: true,
                  fillColor: Colors.red,

                  prefixIcon: Icon(Icons.email,color: Colors.white,),
                  border: InputBorder.none,
                  hintText: "Username",
              ),
            ),
            SizedBox(height: 25,),
            TextField(
              controller: passctr,


              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.red,

                prefixIcon: Icon(Icons.lock,color: Colors.white,),
                border: InputBorder.none,
                hintText: "Password",
              ),
            ),
            SizedBox(height: 25,),

            SizedBox(
            height: 40,
            width: size.width,
    child: RaisedButton(
    color: Colors.green,
    elevation: 0,
    focusElevation: 0,
    highlightElevation: 3,
    shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(15)),
    onPressed: (){

      print(usrctr.text);
      print(passctr.text);


      if(usrctr.text=="admin@olympica.in"&& passctr.text == "admin1"){
        Get.to(AdminHome());
      }

    },
    child: FittedBox(
    fit:  BoxFit.contain,
    child:
    Text(
    "Log In",
    style: TextStyle(fontSize: 20,color: Colors.black),
    ),
    ),
    textColor: Colors.white,
    ),
    ),




          ],
        ),
      )
    );
  }
}
