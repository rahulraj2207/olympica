import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';
import 'package:sportsgram/screens/Auth/Controllers/getGroundListController.dart';
import 'package:sportsgram/utils/utils/theme.dart';
import 'package:sportsgram/utils/utils/widgets.dart';

class AdminHome extends StatefulWidget {
  @override
  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {


  final GetListControllerU getListControllerAdmin = Get.find();
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(



          child:Column(
            children: [
SizedBox(height: 25,),
              Text("Grounds",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
              SizedBox(height: 25,),

              Container(
                height: size.height,
                width: size.width/1.03,
                child: Obx(
                      () {

                    if (getListControllerAdmin.groundList.isEmpty)

                      return Center(child: CircularProgressIndicator());
                    else
                      return ListView.builder(
                        controller: _scrollController,
                        padding: const EdgeInsets.only(bottom: 25, top: 15),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: getListControllerAdmin.groundList.length,
                        itemBuilder: (context, i)
                        {

                          return Padding(
                            padding:   EdgeInsets.only(left: i == 0 ? size.width * 0.04 : 15,bottom: 25),

                            child: AdminCard(
                              eventHeaderImage: getListControllerAdmin.groundList[i].gimage,
                              about: getListControllerAdmin.groundList[i].gabout,
                              details: getListControllerAdmin.groundList[i].gdetails,
                              groundName: getListControllerAdmin.groundList[i].gname,

                            ),
                          );


                        },
                      );
                  },
                ),
              ),


            ],
          ),


        ),
      ),
      
    );
  }
}





class AdminCard extends StatelessWidget {
  final String eventHeaderImage;
  final String about;
  final String details;
  final String groundName;

  const AdminCard(
      {Key key,
        this.eventHeaderImage,
        this.about,
        this.details,
        this.groundName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = 312;
    return SizedBox(
        height: height.toDouble(),
        width: 230,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: cWhite,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 10,
                color: cDarkGrey.withOpacity(0.2),
                spreadRadius: 5,
                offset: const Offset(-5, 10),
              ),
            ],
          ),
          child: Column(
            children: [
              SizedBox(
                height: (height / 2) - 20,
                child: ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  child: Stack(
                    children: [
                      if (eventHeaderImage != null)
                        Image.network(
                          eventHeaderImage,
                          fit: BoxFit.cover,
                          width: double.maxFinite,
                        ),
                      const Positioned.fill(
                        child: DecoratedBox(
                          decoration: BoxDecoration(color: Colors.black38),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SizedBox(
                              height: 25,
                              child: DecoratedBox(
                                decoration: BoxDecoration(
                                    color: cWhite,
                                    borderRadius: BorderRadius.circular(25)),

                              )),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [

                            const SizedBox(width: 4),
                            Expanded(
                                child: buildCaptions(
                                    text:details??"",
                                    alignment: Alignment.centerLeft))
                          ],
                        ),
                        Text(
                          groundName??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        Text(
                          about??"",
                          textAlign: TextAlign.left,
                          style: Get.textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),

                        TextButton(onPressed: (){



                        }, child: Text("Approve"))



                        // )
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }



}
