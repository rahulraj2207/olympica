// import 'dart:convert';
//
import 'package:bot_toast/bot_toast.dart';
import 'package:get/get.dart';
import 'package:sportsgram/utils/utils/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:sportsgram/owner/ownerAccount.dart';



class OwnerLoginController extends GetxController {



  String emailUserMob;
  String password;

  final _auth = FirebaseAuth.instance;


  //!SIgnUp Validations
  var emailUserMobError = 'review'.obs;
  var passwordError = 'review'.obs;

  void validateEmailUserMob(String val) {
    emailUserMob = val;
    update();
    if (val == null) {
      print("email");
      emailUserMobError.value = 'Enter your Email ';
    } else if (val.trim().isEmpty) {
      print("email1");

      emailUserMobError.value = 'Enter your Email';
    } else {
      emailUserMobError.value = 'success';
    }
  }


  void validatePassword(String val) {
    password = val;
    update();
    if (val == null) {
      print("pwrd");

      passwordError.value = 'Enter your Password';
    } else if (val.trim().isEmpty) {
      print("pwrd1");

      passwordError.value = 'Enter your Password';
    } else {
      passwordError.value = 'success';
    }
  }



  void validateRahul1()async{


    validateEmailUserMob(emailUserMob);
    validatePassword(password);


    if (emailUserMobError.value != 'success') {
      showMyErrorSnakBar(
          title: emailUserMobError.value, caption: 'Login Failed');
    } else if (passwordError.value != 'success') {
      showMyErrorSnakBar(title: passwordError.value, caption: 'Login Failed');
    }else{
      botLoading();
      try{

        final user = await _auth.signInWithEmailAndPassword(email: emailUserMob, password: password);
        if(user!=null){

          loginSuccessToast("Login successful");
          print(emailUserMob);
          Get.offAll(() => OwnerAccount(),transition: Transition.cupertino);
        }else{
          print("elseinif");
        }
      }catch(e){
        print(emailUserMobError);
        print(passwordError);
        print(emailUserMob);
        print(password);
        BotToast.closeAllLoading();
        loginFailedToast("Login Failed");
        print("why");
        print(e);
      }
    }
  }




  // void validateLogin() {
  //   print(emailUserMob);
  //   validateEmailUserMob(emailUserMob);
  //   validatePassword(password);
  //   if (emailUserMobError.value != 'success') {
  //     showMyErrorSnakBar(
  //         title: emailUserMobError.value, caption: 'Login Failed');
  //   } else if (passwordError.value != 'success') {
  //     showMyErrorSnakBar(title: passwordError.value, caption: 'Login Failed');
  //   } else {
  //     botLoading();
  //     // LoginApi().login(loginId: emailUserMob,password: password).then((response) async {
  //   var responseMap =
  //   json.decode(response.body) as Map<String, dynamic>;
  //   if (response.statusCode >= 400) {
  //     await loginFailedToast(responseMap['app_data']);
  //   } else {
  //     Get.find<UserDataController>().accessTokenFromJson(response.body);
  //
  //     await AuthStorage().writeData(key: 'Login-session', value: 'success');
  //     await AuthStorage().writeData(key: 'LoginId', value: emailUserMob);
  //     await AuthStorage().writeData(key: 'password', value: password);
  //     // ignore: unawaited_futures
  //     Get.offAll(HomeScreen(),transition: Transition.cupertino);
  //     // loginSuccessToast("Login successfull");
  //     BotToast.closeAllLoading();
  //   }
  // });
  //   }
  // }

  Future<void> loginFailedToast(String error) {
    botSuccessMsg(sucessMsg: error, isSuccess: false);
    return Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }

  void loginSuccessToast(String msg) {
    botSuccessMsg(sucessMsg: msg, isSuccess: true);
    Future.delayed(const Duration(seconds: 3))
        .then((value) => BotToast.closeAllLoading());
  }
}
