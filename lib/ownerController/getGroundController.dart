import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:get/get.dart';
import 'package:sportsgram/firebase/groundFirebase.dart';


class GetListControllerO extends GetxController {


  GetListControllerO(
      {
        this.email,
      });
  final String email;



  var ownerList = List<Ground>().obs;



  @override
  void onInit() {
    super.onInit();
  }

  void getOwnergroundList(String em)async {
    if (await GroundStore().databaseO(em) != null) {

      for (var grounds in await GroundStore().getFromDbU()) {
        ownerList.add(grounds);
      }
    }
  }
}

