import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthStorage {
  final storage = const FlutterSecureStorage();
  Future writeData({@required String key, @required String value}) async {
    final response = await storage.write(key: key, value: value);
    return response;
  }

  Future<String> readData({@required String key}) async {
    final response = await storage.read(key: key);
    return response;
  }

  Future deleteData({@required String key}) async {
    final response = await storage.delete(key: key);
    return response;
  }
}
